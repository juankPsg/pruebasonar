﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using ExLide.Static;
using Microsoft.Office.Interop.PowerPoint;

namespace ExLide.Model.Outline
{
    /// <summary>
    /// @yosef  29/12/19
    /// defines model of drawing outline
    /// </summary>
    public class ExDrawingOutline: Outline
    {
        #region define Singleton Pattern

        /// <summary>
        /// @yosef  29/12/19
        /// instance of DrawingOutline
        /// </summary>
        private static ExDrawingOutline instance;

        /// <summary>
        /// @yosef  29/12/19
        /// private constructor
        /// </summary>
        private ExDrawingOutline()
        {

        }

        /// <summary>
        /// @yosef  29/12/19
        /// get instance of DrawingOutline
        /// </summary>
        public static ExDrawingOutline Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new ExDrawingOutline();
                }
                return instance;
            }
        }

        #endregion

        #region define properties of outline

        /// <summary>
        /// @yosef  29/12/19
        /// instance of DrawingOutline
        /// </summary>
        public Shape Outline { get; set; } = null;

        /// <summary>
        /// @yosef  29/12/19
        /// x value of LeftTop corner of drawing outline
        /// </summary>
        public float Left { get; set; }

        /// <summary>
        /// @yosef  29/12/19
        /// y value of LeftTop corner of drawing outline
        /// </summary>
        public float Top { get; set; }

        /// <summary>
        /// @yosef  29/12/19
        /// width of outline
        /// </summary>
        public float Width { get; set; }

        /// <summary>
        /// @yosef  29/12/19
        /// height of outline
        /// </summary>
        public float Height { get; set; }

        /// <summary>
        /// @yosef  29/12/19
        /// prefered width of the outline
        /// </summary>
        public float preferedWidth { get; } = 320;

        /// <summary>
        /// @yosef  29/12/19
        /// prefered height of the outline
        /// </summary>
        public float preferedHeight { get; } = 180;

        /// <summary>
        /// @yosef  29/12/19
        /// border color of the outline
        /// </summary>
        public Color borderColor { get; } = ColorUtils.getColor("#86ded6");

        /// <summary>
        /// @yosef  29/12/19
        /// border width of the outline
        /// </summary>
        public int borderWidth { get; } = 1;

        /// <summary>
        /// @yosef
        /// fill color of the outline
        /// </summary>
        public Color fillColor { get; } = ColorUtils.getColor("#c1eeea");

        #endregion
    }
}
