﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExLide.Model.DataLayer
{
    
    public class PresentationData : IExMetaData
    {
        private List<GridData> documentGrids = new List<GridData>();
        private JObject ExlideDocumentData = new JObject();

        #region Properties
        public string Id {
            get { return ExlideDocumentData["id"].ToString(); }
            set { ExlideDocumentData["id"] = value; } 
        }
        #endregion


        public PresentationData()
        {
            ExlideDocumentData["version"] = "1.0";
            ExlideDocumentData["id"] = "";
            JArray Grids = new JArray();
            ExlideDocumentData["grids"] = Grids;
        }

        #region IExMetaData implementation
        public JObject ToJSON()
        {
            JArray storedGrids = new JArray();
            if (documentGrids.Count >= 0)
            {
                foreach (GridData gridData in documentGrids)
                {
                    storedGrids.Add(gridData.ToJSON());
                }
            }

            ExlideDocumentData["grids"] = storedGrids;
            return ExlideDocumentData;
        }

        public void ImportFromJObject(JObject metaData)
        {
            this.ExlideDocumentData["version"] = (string)metaData["version"];
            this.ExlideDocumentData["licenceId"] = (string)metaData["licenceId"];
            JArray storedGrids = (JArray)metaData["grids"];

            if (null != storedGrids)
            {
                foreach (JObject obj in storedGrids)
                {
                    GridData gridData = new GridData();
                    //TODO: Add Try Catch and thrown an Exception in ImportFromObject if can't map the object
                    gridData.ImportFromJObject(obj);
                    documentGrids.Add(gridData);
                }
            }
        }

        #endregion

        public void AddGrid(GridData gridData)
        {
            this.documentGrids.Add(gridData);
        }

        public GridData GetGridDataById(string gridId)
        {
            if (documentGrids.Count <= 0)
            {
                return null;
            }
            foreach (GridData gridData in documentGrids)
            {
                if (gridData.Id == gridId)
                {
                    return gridData;
                }
            }
            return null;
        }

    }
}
