﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExLide.Model.DataLayer
{
    public class GridData : IExMetaData 
    {
        #region Phisical Properties
        public float Width { get; set; } = 100;
        public float Height { get; set; } = 100;
        public float LeftPosition { get; set; } = 0;
        public float TopPosition { get; set; } = 0;
        public int Cols { get; set; } = 1;
        public int Rows { get; set; } = 1;

        public float CellWidth { 
            get {
                return Width / Cols;
            } 
        }
        public float CellHeight
        {
            get
            {
                return Height / Rows;
            }
        }
        #endregion

        #region
        public string Id { get; set; }
        public string CreationDate { get; set; }
        public string TemplateId { get; set; }
        #endregion
        public GridData()
        {

        }

        /// <summary>
        /// Return JObject with the Grid information
        /// </summary>
        /// <returns></returns>
        public JObject ToJSON()
        {
            var gridJsonOjbect = new JObject();
            gridJsonOjbect["id"] = Id;
            gridJsonOjbect["cols"] = Cols;
            gridJsonOjbect["rows"] = Rows;
            gridJsonOjbect["template"] = TemplateId;
            return gridJsonOjbect;
        }

        public void ImportFromJObject(JObject obj)
        {
            if(null != obj["id"] && null != obj["cols"] && null != obj["rows"] && null != obj["template"])
            {
                Id = obj["id"].ToString();
                Cols = (int) obj["cols"];
                Rows = (int)obj["rows"];
                TemplateId = obj["template"].ToString();
            }
        }

        /// <summary>
        /// Business Validation
        /// </summary>
        /// <returns></returns>
        public bool isValid()
        {
            if(0 == Cols || 0 == Rows)
            {
                return false;
            }
            return true;
        }

    }
}
