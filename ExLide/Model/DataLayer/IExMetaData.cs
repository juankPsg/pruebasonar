﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExLide.Model.DataLayer
{
    interface IExMetaData
    {
        JObject ToJSON();
        void ImportFromJObject(JObject obj);
    }

    
}
