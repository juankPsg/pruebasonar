﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Office.Interop.PowerPoint;

namespace ExLide.Model.PowerPoint
{
    /// <summary>
    /// @yosef  29/12/19
    /// <Quote From https://docs.microsoft.com/en-us/office/vba/api/powerpoint.documentwindow>
    /// Represents a document window. The DocumentWindow object is a member of the DocumentWindows
    /// collection. The DocumentWindows collection contains all the open document windows
    /// </Quote>
    /// </summary>
    public class ExDocumentWindow
    {
        /// <summary>
        /// @yosef  29/12/19
        /// instance of the Singleton Object
        /// </summary>
        private static ExDocumentWindow instance;

        /// <summary>
        /// @yosef  29/12/19
        /// private constructor
        /// </summary>
        private ExDocumentWindow()
        {
            docWindow = ExPresentation.Instance.CurrentPresentation.Windows[1];
            SplitHorizontal = docWindow.SplitHorizontal;
            SplitVertical = docWindow.SplitVertical;
            Zoom = docWindow.View.Zoom;
        }

        /// <summary>
        /// @yosef  29/12/19
        /// Get instance of the Singleton Object
        /// </summary>
        public static ExDocumentWindow Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new ExDocumentWindow();
                }
                return instance;
            }
        }

        /// <summary>
        /// @yosef  29/12/19
        /// instance of document window
        /// <Quote from https://docs.microsoft.com/en-us/office/vba/api/powerpoint.documentwindow.splithorizontal>
        /// The percentage of the document window width that the outline pane occupies.
        /// </Quote>
        /// </summary>
        public DocumentWindow docWindow { get; } = null;

        /// <summary>
        /// @yosef  29/12/19
        /// </summary>
        public int SplitHorizontal { get; } = 0;

        /// <summary>
        /// @yosef  29/12/19
        /// <Quote from https://docs.microsoft.com/en-us/office/vba/api/powerpoint.documentwindow.splitvertical>
        /// The percentage of the doucment window height that the slide pane occupies.
        /// </Quote>
        /// </summary>
        public int SplitVertical { get; } = 0;

        /// <summary>
        /// @yosef  29/12/19
        /// <Quote from https://docs.microsoft.com/en-us/office/vba/api/powerpoint.view.zoom>
        /// Zoom Setting of the specified view as a percentage of normal size.
        /// </Quote>
        /// Zomm is an integer variable that has min value(10) and max value(100).
        /// This var should be a var of ExView which  is a member var of ExDocumentWindow,
        /// but only zoom var is used in the ExView now, so thsi var is define here,
        /// and this var might be define in the ExView class in the future.
        /// </summary>
        public int Zoom { get; } = 10;
    }
}
