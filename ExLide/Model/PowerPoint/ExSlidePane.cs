﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ExLide.Controllers;
using ExLide.Static;
using Gma.System.MouseKeyHook;

namespace ExLide.Model.PowerPoint
{
    /// <summary>
    /// @yosef  29/12/19
    /// Defines SlidePane Class
    /// </summary>
    public class ExSlidePane
    {
        #region define layout variables
        /// <summary>
        /// @yosef  29/12/19
        /// value of X-coordinate of the LeftTopCorner of the SlidePane
        /// absolutely in the screen-pixel-unit
        /// </summary>
        public float Left { get; } = 0;

        /// <summary>
        /// @yosef  29/12/19
        /// value of Y-coordinate of the LeftTopCorner of the SlidePane, 
        /// absolutely in the screen-pixel-unit
        /// </summary>
        public float Top { get; } = 0;

        /// <summary>
        /// @yosef  29/12/19
        /// width of the SlidePane, absolutely in the screen-pixel-unit
        /// </summary>
        public float Width { get; } = 0;

        /// <summary>
        /// @yosef  29/12/19
        /// height of the SlidePane, absolutely in the screen-pixel-unit
        /// </summary>
        public float Height { get; } = 0;
        #endregion define layout variables

        #region define Singleton
        /// <summary>
        /// @yosef  29/12/19
        /// instance of the singleton class
        /// </summary>
        private static ExSlidePane instance = null;

        /// <summary>
        /// @yosef  29/12/19
        /// constructor
        /// </summary>
        private ExSlidePane()
        {
            // @yosef   29/12/19
            // ToDo 1: Calculate the Left, Top, Width, Height of the SlidePane

            // ToDo 1 - 1: Get Rectangle of the SlideView
            Native.RECT slideViewRect;
            IntPtr slideViewPtr = ExPresentation.Instance.ppSlideView;
            Native.GetWindowRect(new HandleRef(new object(), slideViewPtr), out slideViewRect);

            // Fix for slideViewRect
            // Move (8, 32)
            // Offset is get while running the add-in
            // this add in might be different in different versions of Office
            //slideViewRect.Left += 8;
            //slideViewRect.Right -= 8;
            //slideViewRect.Top += 32;
            //slideViewRect.Bottom -= 8;

            // ToDo 1 - 2: Get SplitHorizontal, SplitVertical of the DocumentWindow
            int splitHor = ExDocumentWindow.Instance.SplitHorizontal;
            int splitVer = ExDocumentWindow.Instance.SplitVertical;

            // ToD 1 - 3: Get Left, Top, Width, Height of the SlidePane
            this.Left = slideViewRect.Left + (slideViewRect.Right - slideViewRect.Left) * splitHor / 100;
            this.Top = slideViewRect.Top;
            this.Width = (slideViewRect.Right - slideViewRect.Left) * (100 - splitHor) / 100;
            this.Height = (slideViewRect.Bottom - slideViewRect.Top) * splitVer / 100;

            // Fix for slidePaneRect
            //this.Left += 8;
            //this.Width -= 16; // because have to decrease both left and right panel
            //this.Top += 32;
            //this.Height -= 40; // becasuse have to decrease both top(32) and bottom(8)
        }

        /// <summary>
        /// @yosef  29/12/19
        /// instance of the SlidePane
        /// </summary>
        public static ExSlidePane Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new ExSlidePane();
                }
                return instance;
            }
        }
        #endregion

        #region MouseEvents

        /// <summary>
        /// @yosef  29/12/19
        /// checks if the cursor is within the slide pane window
        /// </summary>
        /// <param name="e">MouseEventExtArgs</param>
        /// <returns>true if mouse cursor is within the slide pane window, otherwise false</returns>
        public bool Contains(MouseEventExtArgs e)
        {
            return e.X > this.Left && e.X < this.Left + this.Width &&
                e.Y > this.Top && e.Y < this.Top + this.Height;
        }

        /// <summary>
        /// @yosef  29/12/19
        /// get cursor position related to the slide pane window
        /// the conversion from (Ax, Ay) to (Rx, Ry) can be found from
        /// E:\powerpoint addin\ExLide_note\CursorPositionInSlidePane.png
        /// </summary>
        /// <returns>related cursor position</returns>
        public Point getPositionInPane()
        {
            ExSlidePane slidePane = Instance;
            ExDocumentWindow docWindow = ExDocumentWindow.Instance;
            ExPresentation presentation = ExPresentation.Instance;

            Point absolutePos = Cursor.Position;
            Point relatedPos = new Point
            {
                X = (int)((absolutePos.X - slidePane.Left -
                (slidePane.Width - presentation.SlideViewWidth * docWindow.Zoom / 100) / 2) / docWindow.Zoom * 100 /
                presentation.SlideViewWidth * presentation.SlideWidth),

                Y = (int)((absolutePos.Y - slidePane.Top -
                (slidePane.Height - presentation.SlideViewHeight * docWindow.Zoom / 100) / 2) / docWindow.Zoom * 100 /
                presentation.SlideViewHeight * presentation.SlideHeight)
            };

            return relatedPos;
        }
        #endregion
    }
}
