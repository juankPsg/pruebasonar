﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Office.Interop.PowerPoint;

namespace ExLide.Model.PowerPoint
{
    /// <summary>
    /// @yosef  29/12/19
    /// Reference to the selcted slide of the current PPT process
    /// This class must be Singleton, 
    /// because only one slide can be considered to add visual grid or template at a time
    /// if there are more than 2 slides selected, the last one will be refered
    /// </summary>
    public class ExSelectedSlide
    {
        /// <summary>
        /// @yosef  29/12/19
        /// instance of the singleton class
        /// </summary>
        private static ExSelectedSlide instance;

        /// <summary>
        /// @yosef  29/12/19
        /// private constructor
        /// </summary>
        private ExSelectedSlide()
        {
        }

        /// <summary>
        /// @yosef  29/12/19
        /// get instance of the singleton
        /// </summary>
        public static ExSelectedSlide Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new ExSelectedSlide();
                }
                return instance;
            }
        }

        /// <summary>
        /// @yosef  29/12/19
        /// Reference to active slide in the current PPT process
        /// </summary>
        private Slide slideRef;

        /// <summary>
        /// @yosef  29/12/19
        /// delegate of the SlideSelctionChangedEvent and set the selected slide
        /// </summary>
        /// <param name="sldRange">contains selectedslide</param>
        public void SetSlideRef(SlideRange sldRange) 
        {
            if (null != sldRange && sldRange.Count > 0 && null != sldRange[1]) { 
                slideRef = sldRange[1];
            }
        }

        /// <summary>
        /// @yosef  29/12/19
        /// returns the selected slide reference
        /// </summary>
        /// <returns>reference to the selected slide</returns>
        public Slide GetSlideRef()
        {
            return slideRef;
        }
    }
}
