﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ExLide.Static;
using Microsoft.Office.Interop.PowerPoint;

namespace ExLide.Model.PowerPoint
{
    /// <summary>
    /// @yosef, 29/12/19
    /// </summary> 
    //[Obsolete ("This funcionality has been implemented in the PowerPointService",false)]
    public class ExPresentation
    {
        /// <summary>
        /// @yosef  29/12/19
        /// instance of the singleton class
        /// </summary>
        private static ExPresentation instance = null;

        /// <summary>
        /// @yosef  29/12/19
        /// constructor of the Presentation
        /// </summary>
        private ExPresentation()
        {
            ppHandle = Process.GetCurrentProcess().MainWindowHandle;
            ppSlideView = ExVersionCompatible.FindSlideViewHandle(ppHandle);
            CurrentPresentation = Globals.ThisAddIn.Application.ActivePresentation;
            SlideWidth = CurrentPresentation.PageSetup.SlideWidth;
            SlideHeight = CurrentPresentation.PageSetup.SlideHeight;

        }

        /// <summary>
        /// @yosef  29/12/19
        /// Instance of the Singleton class
        /// </summary>
        public static ExPresentation Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new ExPresentation();
                }
                return instance;
            }
        }

        #region define member vars
        /// <summary>
        /// @yosef  29/12/19
        /// handle of the powerpoint process
        /// </summary>
        private IntPtr ppHandle = IntPtr.Zero;

        /// <summary>
        /// @yosef  29/12/19
        /// handle of the slideview
        /// in normal view, it consistsof 1) SlidePane 2) NotesPane 3)OutlinePane
        /// </summary>
        public IntPtr ppSlideView { get; } = IntPtr.Zero;

        /// <summary>
        /// @yosef  29/12/19
        /// instance of the presenation of the current process
        /// </summary>
        public Presentation CurrentPresentation { get; }

        /// <summary>
        /// @yosef  29/12/19
        /// SlideWidth of PageSetup in Presentation
        /// this field might be defined in the different Class
        /// </summary>
        public float SlideWidth { get; } = 0;

        /// <summary>
        /// @yosef  29/12/19
        /// SlideHeight of PageSetup in Presentation
        /// this field might be defined in the different Class
        /// </summary>
        public float SlideHeight { get; } = 0;

        /// <summary>
        /// @yosef  3/1/20
        /// View Width of the Slide
        /// this field must be fixed to get dynamically in the future
        /// </summary>
        public float SlideViewWidth { get; } = 1280;

        /// <summary>
        /// @yosef  3/1/20
        /// View Height of the Slide
        /// this field must be fixed to get dynamically in the future
        /// </summary>
        public float SlideViewHeight { get; } = 720;
        #endregion
    }
}
