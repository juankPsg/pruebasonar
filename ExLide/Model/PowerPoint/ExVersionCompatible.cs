﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ExLide.Static;

namespace ExLide.Model.PowerPoint
{
    /// <summary>
    /// @yosef, 29/12/19
    /// This class defines functions of its implementation is different accroding to the Office Version
    /// The constructor will be private, because this class has none object
    /// </summary>
    public class ExVersionCompatible
    {
        /// <summary>
        /// @yosef, 29/12/19
        /// private constructor
        /// </summary>
        private ExVersionCompatible()
        {

        }

        /// <summary>
        /// @yosef, 29/12/19
        /// For Office 2010, its window structure is like MDIClient --> mdiClass --> paneClassDC (SlideView)
        /// For Office 2013, it's like MDIClient --> mdiClass (SlideView)
        /// The SlideView consist of Outline Pane, Slide Pane and Notes Pane
        /// this structure can be found using SPY++ provided by visual studio
        /// </summary>
        /// <param name="ppHandle">Handle of the current powerpoint process</param>
        /// <returns>Handle of the SlideView</returns>
        public static IntPtr FindSlideViewHandle(IntPtr ppHandle)
        {
            IntPtr slideViewHandle = IntPtr.Zero;
            IntPtr mdiClient = Native.FindWindowEx(ppHandle, IntPtr.Zero, "MDIClient", "");
            if (mdiClient != IntPtr.Zero)
            {
                IntPtr mdiClass = Native.FindWindowEx(mdiClient, IntPtr.Zero, "mdiClass", "");
                if (mdiClass != IntPtr.Zero)
                {
                    slideViewHandle = Native.FindWindowEx(mdiClass, IntPtr.Zero, "panelClassDC", "Slide");
                    if (slideViewHandle == IntPtr.Zero)
                    {
                        slideViewHandle = mdiClass;
                    }
                }
            }
            return slideViewHandle;
        }
    }
}
