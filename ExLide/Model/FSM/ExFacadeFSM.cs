﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExLide.Model.FSM
{
    /// <summary>
    /// @yosef  29/12/19
    /// defines Global FSM
    /// </summary>
    public class ExFacadeFSM
    {
        #region Singleton Pattern

        /// <summary>
        /// @yosef  29/12/19
        /// instance of the GlobalFSM
        /// </summary>
        private static ExFacadeFSM instance;
        
        /// <summary>
        /// @yosef  29/12/19
        /// private constructor
        /// </summary>
        private ExFacadeFSM()
        {

        }

        /// <summary>
        /// @yosef  29/12/19
        /// get instance of the GlobalFSM
        /// </summary>
        public static ExFacadeFSM Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new ExFacadeFSM();
                }
                return instance;
            }
        }

        #endregion
    }
}
