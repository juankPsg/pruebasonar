﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExLide.Model.FSM
{
    /// <summary>
    /// @yosef  29/12/19
    /// define global fsm
    /// </summary>
    public class ExGlobalFSM
    {
        #region define Singleton Pattern

        /// <summary>
        /// @yosef  29/12/19
        /// instance of GlobalFSM
        /// </summary>
        private static ExGlobalFSM instance = null;

        /// <summary>
        /// @yosef  29/12/19
        /// private constructor
        /// </summary>
        private ExGlobalFSM()
        {

        }

        /// <summary>
        /// @yosef  29/12/19
        /// get instance of the GlobalFSM
        /// </summary>
        public static ExGlobalFSM Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new ExGlobalFSM();
                }
                return instance;
            }
        }
        #endregion

        #region define global fsm state

        /// <summary>
        /// @yosef  29/12/19
        /// define global fsm state
        /// </summary>
        public enum GLOBAL_FSM_STATE
        {
            GFS_FACADE,
            GFS_OUTLINE,
        }

        /// <summary>
        /// @yosef  29/12/19
        /// current global fsm state
        /// </summary>
        public GLOBAL_FSM_STATE currentState { get; set; }

        #endregion
    }
}
