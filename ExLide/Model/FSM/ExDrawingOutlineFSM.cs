﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ExLide.Model.PowerPoint;

namespace ExLide.Model.FSM
{
    /// <summary>
    /// @yosef  29//12/19
    /// FSM of the Outline, defines lifecycle and behaviors of the Outline
    /// </summary>
    public class ExDrawingOutlineFSM
    {
        #region define State of the FSM
        /// <summary>
        /// @yosef  26/12/19
        /// <FSM Description>
        /// <State "DOLS_NONE">
        /// ~~ Not Initialized
        /// </State>
        /// <State "DOLS_HIDDEN">
        /// ~~ Init State of the FSM
        /// </State> 
        /// <State "DOLS_PREFRED">
        /// ~~ Before start drawing outline, 
        /// it will show outline having prefered width and height
        /// </State>
        /// <State "DOLS_DRAW">
        /// ~~ Drawing Outline state,
        /// it will show outline having the mouse-moved width and height
        /// </State>
        /// <State "DOLS_ADDSHP">
        /// ~~ This actually note state,
        /// it will add a custom shape of defined type
        /// it will finish this FSM and will returned to another Sub FSM.
        /// </State>
        /// <State "DOLS_CANCEL">
        /// ~~ This actually note state,
        /// the outline drawing or inserting a custom shape,
        /// will be canceled if click the right button or pressing Esc Key
        /// while drawing the outline
        /// it will finish this FSM and will returned to Global FSM.
        /// </State>
        /// </FSM>
        /// </summary>
        public enum ExDrawingOutlineState
        {
            DOLS_NONE,
            DOLS_HIDDEN,
            DOLS_PREFERED,
            DOLS_DRAW,
            DOLS_CANCEL,
        }

        /// <summary>
        /// state of the Outline FSM
        /// </summary>
        private ExDrawingOutlineState dols_state;
        #endregion

        #region define Singleton Pattern

        /// <summary>
        /// @yosef  29/12/19
        /// instance of the Outline FSM
        /// </summary>
        private static ExDrawingOutlineFSM instance = null;

        /// <summary>
        /// @yosef  29/12/19
        /// private constructor
        /// </summary>
        private ExDrawingOutlineFSM()
        {

        }

        /// <summary>
        /// @yosef  29/12/19
        /// get instance of the Singleton Object
        /// </summary>
        public static ExDrawingOutlineFSM Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new ExDrawingOutlineFSM();
                }
                return instance;
            }
        }

        #endregion

        #region define state variables

        #endregion

        #region define state input

        public enum ExDrawingOutlineInput
        {
            DOLI_MOVEINPANE,
            DOLI_MOVEOUTPANE,
            DOLI_LBTNDOWN,
            DOLI_LBTNUP,
        }

        #endregion

        #region manage state 
        /// <summary>
        /// @yosef  29/12/19
        /// Activate Outline FSM
        /// will set the state to OLS_HIDDEN which is the init state of this FSM
        /// and will initialized some vars defined in <define state vars/> region
        /// </summary>
        public void Init()
        {
            dols_state = ExDrawingOutlineState.DOLS_HIDDEN;
        }

        public void SetState(ExDrawingOutlineState exDrawingOutlineState)
        {
            switch (exDrawingOutlineState)
            {
                case ExDrawingOutlineState.DOLS_NONE:
                    dols_state = ExDrawingOutlineState.DOLS_NONE;
                    break;
                case ExDrawingOutlineState.DOLS_HIDDEN:
                    dols_state = ExDrawingOutlineState.DOLS_HIDDEN;
                    break;
                case ExDrawingOutlineState.DOLS_PREFERED:
                    dols_state = ExDrawingOutlineState.DOLS_PREFERED;
                    break;
                case ExDrawingOutlineState.DOLS_DRAW:
                    dols_state = ExDrawingOutlineState.DOLS_DRAW;
                    break;
                case ExDrawingOutlineState.DOLS_CANCEL:
                    dols_state = ExDrawingOutlineState.DOLS_CANCEL;
                    break;
            }
        }

        /// <summary>
        /// @yosef  29/12/19
        /// returns current outline state defined in FSM
        /// </summary>
        /// <returns>current outline state</returns>
        public ExDrawingOutlineState getCurrentState()
        {
            return dols_state;
        }

        /// <summary>
        /// change current state of DrawingOutlineFSM
        /// </summary>
        /// <param name="doli_input">input to the FSM</param>
        public void EmitInput(ExDrawingOutlineInput doli_input)
        {
            switch (dols_state)
            {
                case ExDrawingOutlineState.DOLS_HIDDEN:
                    switch (doli_input)
                    {
                        case ExDrawingOutlineInput.DOLI_MOVEINPANE:
                            dols_state = ExDrawingOutlineState.DOLS_PREFERED;
                            break;
                    }
                    break;
                case ExDrawingOutlineState.DOLS_PREFERED:
                    switch (doli_input)
                    {
                        case ExDrawingOutlineInput.DOLI_MOVEOUTPANE:
                            dols_state = ExDrawingOutlineState.DOLS_HIDDEN;
                            break;
                        case ExDrawingOutlineInput.DOLI_LBTNDOWN:
                            dols_state = ExDrawingOutlineState.DOLS_DRAW;                            
                            break;
                    }
                    break;
                case ExDrawingOutlineState.DOLS_DRAW:
                    switch (doli_input)
                    {
                        case ExDrawingOutlineInput.DOLI_LBTNUP:
                            dols_state = ExDrawingOutlineState.DOLS_NONE;                            
                            break;
                    }
                    break;
            }
        }
        #endregion
    }
}
