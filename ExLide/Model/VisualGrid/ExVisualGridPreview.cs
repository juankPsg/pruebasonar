﻿using ExLide.Controllers;

namespace ExLide.Model.VisualGrid
{
    public class ExVisualGridPreview
    {
        public ExVisualGridMetaData.ExVisualGridType vg_type { get; set; }
        public string iconResourceRef { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="vgType"></param>
        public void setVGType(ExVisualGridMetaData.ExVisualGridType vgType = ExVisualGridMetaData.ExVisualGridType.BLANK)
        {
            this.vg_type = vgType;
            notifyVGTypeUpdate();
        }

        /// <summary>
        /// constructor
        /// 
        /// </summary>
        /// <param name="vgType"></param>
        public  ExVisualGridPreview(ExVisualGridMetaData.ExVisualGridType vgType = ExVisualGridMetaData.ExVisualGridType.BLANK)
        {
            setVGType(vgType);
        }

        protected void notifyVGTypeUpdate()
        {
            this.iconResourceRef = ResourceManager.Instance.GetVGPreviewIconFileName(this.vg_type);
        }

        public string getIconResourceRef()
        {
            return iconResourceRef;
        }
    }
}
