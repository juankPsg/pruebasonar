﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExLide.Model.VisualGrid
{
    // define meta data of Visual Grid
    public class ExVisualGridMetaData
    {
        public const int MAX_ROW = 10;
        public const int MAX_COL = 12;
        public const int GRID_RECT_MARGIN = 1;
        public const bool SHOW_RC_NUM = true;
        public static TextBoxFormat RectGridNumFormat = new TextBoxFormat(0, "#ffffff", "#a0aec0", "#feebc8", "#ed8936", 10);
        public static RectFormat RectGridFormat = new RectFormat(1, "#ffffff", "#a0aec0", "#feebc8", "#ed8936");

        // define the Visual Grid Type
        public enum ExVisualGridType
        {
            BLANK,
            STATUS_STYLE,
            HEADERS_ON_ROWS_AND_COLUMNS,
        }

        // define TextBoxFormat
        public struct TextBoxFormat
        {
            public double Thickness;
            public string FillColorString;
            public string ForegroundColorString;
            public string HighlightFillColorString;
            public string HighlightForegroundColorString;
            public int FontSize;
            // ed8936

            public TextBoxFormat(int _Thickness, string _FillColorString, string _ForegroundColorString,
                string _HighlightFillColorString, string _HighlightForegroundColorString, int _FontSize)
            {
                Thickness = _Thickness;
                FillColorString = _FillColorString;
                ForegroundColorString = _ForegroundColorString;
                HighlightFillColorString = _HighlightFillColorString;
                HighlightForegroundColorString = _HighlightForegroundColorString;
                FontSize = _FontSize;
            }
        };

        // define struct RectFormat
        public struct RectFormat
        {
            public double StrokeThickness;
            public string FillColorString;
            public string StrokeColorString;
            public string HighlightFillColorString;
            public string HighlightStrokeColorString;

            public RectFormat(double _StrokeThickness, string _FillColorString, string _StrokeColorString,
                string _HighlightFillColorString, string _HighlightStrokeColorString)
            {
                StrokeThickness = _StrokeThickness;
                FillColorString = _FillColorString;
                StrokeColorString = _StrokeColorString;
                HighlightFillColorString = _HighlightFillColorString;
                HighlightStrokeColorString = _HighlightStrokeColorString;
            }
        }
    }
}
