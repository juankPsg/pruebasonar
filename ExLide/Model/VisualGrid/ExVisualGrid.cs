﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using ExLide.Controllers;

namespace ExLide.Model.VisualGrid
{    
    public abstract class ExVisualGrid
    {
        public enum GRID_STATUS
        {
            DRAWING,
            SELECTED,
            SHOW,
            DELETED,
        }

        public struct Header
        {
            public string caption;
            public double start_pos;
            public double width;
        }

        public struct Category
        {
            public string caption;
            public double start_pos;
            public double height;
        }

        public struct Cell
        {
            public string content;
        }


        protected int rows;
        protected int cols;
        protected int zorder;
        protected Header[] header;
        protected Category[] category;
        protected Cell[][] cell;

        protected Point position;
        protected Size size;


        protected ExVisualGridMetaData.ExVisualGridType vg_type;
        protected string iconResourceRef;
        public void setVGType(ExVisualGridMetaData.ExVisualGridType vgType)
        {
            this.vg_type = vgType;
            notifyVGTypeUpdate();
        }

        protected void notifyVGTypeUpdate()
        {
            this.iconResourceRef = ResourceManager.Instance.GetVGPreviewIconFileName(this.vg_type);
        }
    }
}
