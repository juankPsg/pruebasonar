﻿using ExLide.Controllers;
using ExLide.Views;
using Microsoft.Office.Tools.Ribbon;

namespace ExLide
{
    public partial class RibbonExLide
    {
        private void RibbonExLide_Load(object sender, RibbonUIEventArgs e)
        {

        }

        private void NewComponentClick(object sender, RibbonControlEventArgs e)
        {
            ComponentController.Instance.CreateTextComponent();
        }

        private void NewExlideGridClick(object sender, RibbonControlEventArgs e)
        {
            // call gridcontroller method to open the template windows
            GridController.Instance.ShowTemplatesCatalogWindow();
        }
    }
}
