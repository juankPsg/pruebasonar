﻿using ExLide.Model.DataLayer;
using ExLide.Services;
using System;
using System.Collections.Generic;
using Shape = Microsoft.Office.Interop.PowerPoint.Shape;
using ShapeRange = Microsoft.Office.Interop.PowerPoint.ShapeRange;

namespace ExLide.Controllers
{
    public sealed class ComponentController
    {
        DocumentMetaDataService documentMetaDataSrv;
        ShapesService shapesSrv;
        GridService gridSrv;
        List<string> componentsName = new List<string>();
        #region define Singleton
        /// <summary> instance of this Singleton Object </summary>
        public static ComponentController instance;
        private static readonly object padlock = new object();

        /// <summary> constructor </summary>
        ComponentController()
        {
            shapesSrv = ShapesService.Instance;
            documentMetaDataSrv = DocumentMetaDataService.Instance;
            gridSrv = GridService.Instance;
        }

        /// <summary> Instance of the GridController </summary>
        public static ComponentController Instance
        {
            get
            {
                lock (padlock)
                {
                    if (instance == null)
                    {
                        instance = new ComponentController();
                    }
                    return instance;
                }
            }
        }
        #endregion

        #region component Methods
        /// <summary>
        /// Gets metadata info and select properties to verify.
        /// TODO change the values to columns and rows to position the element into grid
        /// </summary>
        public void CreateTextComponent()
        {
            int column = 2; 
            int row = 2;
            ShapeRange gridObj = shapesSrv.GetSelectedShape();
            if( null == gridObj)
            {
                return;
            }
            //Change: Get the GridData.
            GridData gridData= documentMetaDataSrv.GetGridDataById(gridObj.Name);
            if(null == gridData)
            {
                return;
            }
            gridData.Width = gridObj.Width;
            gridData.Height = gridObj.Height;
            float cellWidth = gridData.CellWidth;

            if (gridData.Cols < column || gridData.Rows < row)
                return;
            
            componentsName.Add(gridObj.Name);

            float left = gridObj.Left + gridData.CellWidth * (column - 1);
            float top = gridObj.Top + gridData.CellHeight * (row - 1);
            CreateTextBoxComponent(left, top, gridData.CellWidth, gridData.CellHeight);
        }
        /// <summary>
        /// Calculate the posicion where the textbox shape element is gonna be created
        /// </summary>
        /// <param name="gridConfig"></param>
        /// <param name="gridObj"></param>
        /// <param name="gridColumnPosition"></param>
        /// <param name="gridRowPosition"></param>
        private void CalculateComponentPositionAndSize(GridData gridData, ShapeRange gridObj, int col, int row)
        {

            float left = Convert.ToInt16(((gridObj.Width / Convert.ToInt32(gridData.Cols)) + gridObj.Left) * (col - 1));
            float top = Convert.ToInt16(((gridObj.Height / Convert.ToInt32(gridData.Rows)) + gridObj.Top) * (row - 1));

            float height = (gridObj.Height / Convert.ToInt32(gridData.Rows)) / 2;
            float width = gridObj.Width / Convert.ToInt32(gridData.Cols);

            // Call teh method to create the new textbox and pass the properties to position the element.
            
        }
        /// <summary>
        /// Create a new textbox shape into a grid
        /// </summary>
        /// <param name="left"></param>
        /// <param name="top"></param>
        /// <param name="width"></param>
        /// <param name="height"></param>
        private void CreateTextBoxComponent(float left, float top, float width, float height)
        {
            Shape textbox = shapesSrv.NewTextBox(left, top, width, height);
            componentsName.Add(textbox.Name);
            // Call GroupGrid method to group the created textbox shape into selected grid.
            if (gridSrv.GroupGrid(PowerPointService.Instance.getCurrentSlide(), componentsName))
                componentsName.Clear();
            else
                //TODO change this to a visual popup message with the error text.
                return;
        }
        #endregion
    }
}
