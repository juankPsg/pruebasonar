﻿using ExLide.Model.DataLayer;
using ExLide.Services;
using ExLide.Views;
using Microsoft.Office.Interop.PowerPoint;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ExLide.Controllers
{
    public sealed class GridController
    {
        private dynamic selectedtemplate;
        PowerPointService powerPointSrv;
        private TemplateForm TemplateForm;
        private Slide currentSlide;
        #region define Singleton
        /// <summary> instance of this Singleton Object </summary>
        public static GridController instance;
        private static readonly object padlock = new object();

        int columns = 0;
        int rows = 0;

        /// <summary> constructor </summary>
        GridController()
        {
            powerPointSrv = PowerPointService.Instance;
        }

        /// <summary>
        /// Set the current columns and rows value choise by user
        /// </summary>
        /// <param name="columns"></param>
        /// <param name="rows"></param>
        public void SetColumnsAndRowsValue(int columns, int rows)
        {
            this.columns = columns;
            this.rows = rows;
        }

        /// <summary> Instance of the GridController </summary>
        public static GridController Instance
        {
            get
            {
                lock (padlock)
                {
                    if (instance == null)
                    {
                        instance = new GridController();
                    }
                    return instance;
                }
            }
        }
        #endregion

        #region Handle visual grid(s)
  
        /// <summary>
        /// This method is called from the ribbon class (Menu)
        /// </summary>
        public void ShowTemplatesCatalogWindow()
        {
            if (!PowerPointService.Instance.IsActivePresentation())
            {
                return;
            }
            // Obtain the current active slide into presentation. 
            if (!PowerPointService.Instance.isSlideSelected())
            {
                return;
            }
            //show windows template form
            if(null == TemplateForm) { 
                TemplateForm = new TemplateForm();
            }
            TemplateForm.Closed += new EventHandler(ClosedTemplateCatalogWindowHandler);
            TemplateForm.Show();
        }

        private void ClosedTemplateCatalogWindowHandler(Object sender, EventArgs eventArgs)
        {
            //TODO
            TemplateForm = null;
        }


        /// <summary>
        /// Set the selected template config.
        /// </summary>
        public void templateSelected(dynamic templateConfig)
        {
            selectedtemplate = templateConfig;
        }


        

        /// <summary> 
        /// Obtain the created grid dimension by user
        /// </summary>
        public void CreateGrid(float left, float top, float width, float height)
        {
            //TODO: get width and height from mouse selection.
            //TODO: get left and top posititon from mouse selection.
            GridData grid = new GridData
            {
                //TODO: Set the templateID selected by the user. If the user doesn't uses a themplate, it will be Default
                TemplateId = "Plantilla001",
                LeftPosition = left,
                TopPosition = top,
                Cols = columns,
                Rows = rows
            };
            if (left == width && top == height)
            {
                grid.Height = powerPointSrv.getHeight() - top;
                grid.Width = powerPointSrv.getWidth() - left;
            }
            else
            {
                grid.Height = height - top;
                grid.Width = width - left;
            }
           // currentSlide = PowerPointService.Instance.getCurrentSlide();
           // GridService.Instance.newGrid(currentSlide, grid);
        }

        public void CreateGrid(int cols, int rows)
        {
            //registrar los métodos de escucha del mouse
            GridData grid = new GridData
            {
                //TODO: Set the templateID selected by the user. If the user doesn't uses a themplate, it will be Default
                TemplateId = "Plantilla001",
                Cols = cols,
                Rows = rows
            };
            GridService.Instance.NewGridUsingMouse(grid);
        }

        #endregion
    }
}