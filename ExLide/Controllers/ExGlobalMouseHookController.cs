﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Gma.System.MouseKeyHook;

namespace ExLide.Controllers
{
    /// <summary>
    /// @yosef  29/12/19
    /// Define GlobalMouseHookController
    /// It attaches different delegate to the mouse events,
    /// according to the HOOK_STAT
    /// </summary>
    public class ExGlobalMouseHookController
    {
        #region define single

        /// <summary>
        /// @yosef  29/12/16    revised
        /// instance of the GlobalMouseHook
        /// </summary>
        private static ExGlobalMouseHookController instance;

        /// <summary>
        /// @yosef  29/12/16    revised
        /// get instance of the singleton object
        /// </summary>
        public static ExGlobalMouseHookController Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new ExGlobalMouseHookController();
                }
                return instance;
            }
        }
        #endregion

        #region constructor & destructor
        /// <summary>
        /// @yosef  29/12/16    revised
        /// private constructor
        /// </summary>
        private ExGlobalMouseHookController()
        {
            m_GlobalHook = Hook.GlobalEvents();
        }

        /// <summary>
        /// @yosef  29/12/16    revised
        /// destructor
        /// </summary>
        ~ExGlobalMouseHookController()
        {
            m_GlobalHook.Dispose();
        }
        #endregion

        #region define member vars

        /// <summary>
        /// @yosef  29/12/19    revised
        /// Global hook of the Keyboard & mouse events
        /// </summary>
        private IKeyboardMouseEvents m_GlobalHook;

        #endregion

        #region manage hook

        /// <summary>
        /// @yosef  29/12/19    revised
        /// Define possible state of mouse hooks
        /// 
        /// </summary>
        public enum HOOK_STAT {
            HOOK_NONE,
            HOOK_OUTLINE,
            HOOK_VG,
        }

        /// <summary>
        /// @yosef  29/12/19    revised
        /// current state of the mouse hooks
        /// </summary>
        private HOOK_STAT curHookStat = HOOK_STAT.HOOK_NONE;

        /// <summary>
        /// @yosef  29/12/19
        /// init GlobalMouseHookController and its state
        /// </summary>
        public void Init()
        {

        }

        /// <summary>
        /// @yosef  29/19/2/19  revised
        /// switch delegate of the mousehook according to the new hookState
        /// </summary>
        /// <param name="hookStat"></param>
        public void SwitchHook(HOOK_STAT hookStat)
        {
            // remove delegate of the prev hook 
            switch (curHookStat)
            {
                case HOOK_STAT.HOOK_OUTLINE:
                    m_GlobalHook.MouseDownExt -= ExDrawingOutlineController.Instance.ExOutlineHookMouseDownExt;
                    m_GlobalHook.MouseMoveExt -= ExDrawingOutlineController.Instance.ExOutlineHookMouseMoveExt;
                    m_GlobalHook.MouseUpExt -= ExDrawingOutlineController.Instance.ExOutlineHookMouseUpExt;
                    break;

                case HOOK_STAT.HOOK_VG:
                    m_GlobalHook.MouseDownExt -= ExVisualGridManager.Instance.ExVGHookMouseDownExt;
                    m_GlobalHook.MouseMoveExt -= ExVisualGridManager.Instance.ExVGHookMouseMoveExt;
                    m_GlobalHook.MouseUpExt -= ExVisualGridManager.Instance.ExVGHookMouseUpExt;
                    break;
            }
            if(curHookStat == hookStat)
            {
                curHookStat = HOOK_STAT.HOOK_NONE;
                return;
            }

            // attach new delegate of the cur hook
            switch (hookStat)
            {
                case HOOK_STAT.HOOK_OUTLINE:
                    m_GlobalHook.MouseDownExt += ExDrawingOutlineController.Instance.ExOutlineHookMouseDownExt;
                    m_GlobalHook.MouseMoveExt += ExDrawingOutlineController.Instance.ExOutlineHookMouseMoveExt;
                    m_GlobalHook.MouseUpExt += ExDrawingOutlineController.Instance.ExOutlineHookMouseUpExt;
                    break;

                case HOOK_STAT.HOOK_VG:
                    m_GlobalHook.MouseDownExt += ExVisualGridManager.Instance.ExVGHookMouseDownExt;
                    m_GlobalHook.MouseMoveExt += ExVisualGridManager.Instance.ExVGHookMouseMoveExt;
                    m_GlobalHook.MouseUpExt += ExVisualGridManager.Instance.ExVGHookMouseUpExt;
                    break;
            }

            curHookStat = hookStat;
        }
        #endregion
    }
}
