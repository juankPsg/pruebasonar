﻿
using System.Windows.Forms;
using ExLide.Model.PowerPoint;
using ExLide.Model.VisualGrid;
using Gma.System.MouseKeyHook;

using PowerPoint = Microsoft.Office.Interop.PowerPoint;

namespace ExLide.Controllers
{
    public class ExVisualGridManager
    {
        #region define Singleton
        /// <summary>
        /// @yosef  3/1/20  marked
        /// instance of this Singleton Object
        /// </summary>
        public static ExVisualGridManager instance;

        /// <summary>
        /// @yosef  3/1/20  marked
        /// constructor
        /// </summary>
        private ExVisualGridManager()
        {

        }

        /// <summary>
        /// @yosef  3/1/20  marked
        /// Instance of the ExVisualGridManager
        /// </summary>
        public static ExVisualGridManager Instance { 
            get
            {
                if (instance == null)
                {
                    instance = new ExVisualGridManager();
                }
                return instance;
            }
        }
        #endregion

        #region MouseEvent hooks
        /// <summary>
        /// @yosef  3/1/20  marked
        /// delegate of the MouseDown
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void ExVGHookMouseDownExt(object sender, MouseEventExtArgs e)
        {

        }

        /// <summary>
        /// @yosef  3/1/20  marked
        /// delegate of the MouseMove
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void ExVGHookMouseMoveExt(object sender, MouseEventExtArgs e)
        {
        }

        /// <summary>
        /// @yosef  3/1/20  marked
        /// delegate of the MouseUp
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void ExVGHookMouseUpExt(object sender, MouseEventExtArgs e)
        {

        }
        #endregion

        #region manage visual grid(s)
        /// <summary>
        /// @yosef  3/1/20
        /// used for the prepareVisualGrid() functions
        /// </summary>
        ExVisualGridMetaData.ExVisualGridType gridType;

        /// <summary>
        /// @yosef  3/1/20
        /// used for the prepareVisualGrid() functions
        /// </summary>
        int headers;

        /// <summary>
        /// @yosef  3/1/20
        /// used for the prepareVisualGrid() functions
        /// </summary>
        int categories;

        /// <summary>
        /// @yosef  3/1/20
        /// prepare(save) a visual grid infos selected in the VisualGridForm
        /// </summary>
        /// <param name="gridType">GridType selected in the VisualGridForm</param>
        /// <param name="headers">Count of Cols selected in the VisualGridForm</param>
        /// <param name="categories">Count of Rows selected in the VisualGridForm</param>
        public void prepareVisualGrid(ExVisualGridMetaData.ExVisualGridType gridType, int headers, int categories)
        {
            this.gridType = gridType;
            this.headers = headers;
            this.categories = categories;
        }

        /// <summary>
        /// @yosef  3/1/20
        /// add a new visual grid
        /// </summary>
        public void addNewVisualGrid()
        {
            // ToDo 1: Add Componenets of VisualGrid (Headers & Categories & Cells)
        }

        #endregion
    }
}
 