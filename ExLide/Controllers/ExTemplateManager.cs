﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Gma.System.MouseKeyHook;

namespace ExLide.Controllers
{
    public class ExTemplateManager
    {
        /// <summary>
        /// instance of the TemplateManager
        /// </summary>
        public static ExTemplateManager instance;


        /// <summary>
        /// constructor
        /// </summary>
        private ExTemplateManager()
        {

        }


        public static ExTemplateManager Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new ExTemplateManager();
                }
                return instance;
            }
        }

        /// <summary>
        /// delegate of the MouseDown
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void ExVGHookMouseDownExt(object sender, MouseEventExtArgs e)
        {
            if (PPMouseController.IsMouseWithinSlideViewWindow())
            {

            }
        }

        /// <summary>
        /// delegate of the MouseMove
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void ExVGHookMouseMoveExt(object sender, MouseEventExtArgs e)
        {

        }

        /// <summary>
        /// delegate of the MouseUp
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void ExVGHookMouseUpExt(object sender, MouseEventExtArgs e)
        {

        }
    }
}
