﻿using System;
using System.Collections;
using System.Collections.Generic;
using ExLide.Model.VisualGrid;

namespace ExLide.Controllers
{
    /// <summary>
    /// @yosef  3/1/20  marked
    /// this class should be a Singleton 
    /// it manages the vg type and used when to init visual grid insert dialog
    /// </summary>
    public class ExVisualGridPreviewManager
    {
        /// <summary>
        /// @yosef  3/1/20  marked
        /// instance of this singleton class
        /// </summary>
        public static ExVisualGridPreviewManager instance = null;

        /// <summary>
        /// @yosef  3/1/20  marked
        /// list of the VisualGridPreview
        /// </summary>
        private List<ExVisualGridPreview> vgPreview;

        /// <summary>
        /// @yosef  3/1/20  marked
        /// the construct should be set to private in the singleton
        /// </summary>
        private ExVisualGridPreviewManager()
        {            
            Array vgTypeArray = Enum.GetValues(typeof(ExVisualGridMetaData.ExVisualGridType));
            vgPreview = new List<ExVisualGridPreview>();
            foreach (ExVisualGridMetaData.ExVisualGridType type in vgTypeArray)
            {
                vgPreview.Add(new ExVisualGridPreview(type));
            }
        }

        /// <summary>
        /// @yosef  3/1/20  marked
        /// define the Instance of the ExVisualGridTypeManager
        /// </summary>
        public static ExVisualGridPreviewManager Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new ExVisualGridPreviewManager();
                }
                return instance;
            }
        }

        /// <summary>
        /// @yosef  3/1/20  marked
        /// returns the list of the gridpreview
        /// </summary>
        /// <returns></returns>
        public List<ExVisualGridPreview> getPreviewList()
        {
            return vgPreview;
        }

        /// <summary>
        /// @yosef  3/1/20
        /// returns the type of the visual grid in the list
        /// </summary>
        /// <param name="index">index within the list</param>
        /// <returns>the type of the visual grid of which is index equals to the param "index"</returns>
        public ExVisualGridMetaData.ExVisualGridType getVisualGridType(int index)
        {
            return vgPreview[index].vg_type;
        }
    }
}
