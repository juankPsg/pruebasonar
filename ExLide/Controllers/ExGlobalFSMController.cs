﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ExLide.Model.FSM;
using static ExLide.Model.FSM.ExGlobalFSM;

namespace ExLide.Controllers
{
    /// <summary>
    /// @yosef  29/12/19
    /// </summary>
    public class ExGlobalFSMController
    {
        #region define Singleton Pattern

        /// <summary>
        /// @yosef  29/12/19
        /// instance of the FSMController
        /// </summary>
        private static ExGlobalFSMController instance;

        /// <summary>
        /// @yosef  29/12/19
        /// private constructor
        /// </summary>
        private ExGlobalFSMController()
        {

        }

        /// <summary>
        /// @yosef  29/12/19
        /// get instance of the FSMController
        /// </summary>
        public static ExGlobalFSMController Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new ExGlobalFSMController();
                }
                return instance;
            }
        }

        #endregion

        #region switch global state

       

        /// <summary>
        /// change global fsm state
        /// </summary>
        /// <param name="newState">new state to set global fsm state</param>
        /// <returns>true if newState is valid and state change succeed</returns>
        public bool SwitchTo(GLOBAL_FSM_STATE newState)
        {
            switch (newState)
            {
                case GLOBAL_FSM_STATE.GFS_OUTLINE:
                    //remove mouse listeners from mouseEvents 
                    ExGlobalMouseHookController.Instance.SwitchHook(ExGlobalMouseHookController.HOOK_STAT.HOOK_OUTLINE);
                    //Change status to DOLS_HIDDEN
                    ExDrawingOutlineFSM.Instance.Init();
                    break;

            }
            ExGlobalFSM.Instance.currentState = newState;
            return true;
        }
        #endregion
    }
}
