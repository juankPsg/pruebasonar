﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ExLide.Model.VisualGrid;

namespace ExLide.Controllers
{    
    class ResourceManager
    {
        private static ResourceManager instance = null;

        private ResourceManager()
        {

        }

        public static ResourceManager Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new ResourceManager();
                }
                return instance;
            }
        }

        #region Visaul Grid
        
        public string GetVGPreviewIconFileName(ExVisualGridMetaData.ExVisualGridType vgType)
        {
            string baseUri = "/ExLide;component/Resources/VisualGrid/";
            switch (vgType)
            {
                case ExVisualGridMetaData.ExVisualGridType.BLANK:
                    return baseUri + "img_vg_blank.png";
                case ExVisualGridMetaData.ExVisualGridType.STATUS_STYLE:
                    return baseUri + "img_vg_status_style.png";
                case ExVisualGridMetaData.ExVisualGridType.HEADERS_ON_ROWS_AND_COLUMNS:
                    return baseUri + "img_vg_headers_on_rows_and_columns.png";
            }

            return string.Empty;
        }
        #endregion
    }
}
