﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ExLide.Model.FSM;
using ExLide.Model.Outline;
using ExLide.Model.PowerPoint;
using ExLide.Views;
using Gma.System.MouseKeyHook;
using Microsoft.Office.Interop.PowerPoint;

namespace ExLide.Controllers
{
    /// <summary>
    /// @yosef  29/12/19
    /// Draw Outlines before add Visual Grid or Template
    /// </summary>
    public class ExDrawingOutlineController
    {
        /// <summary>
        /// @yosef  29/12/19
        /// instance of singletone
        /// </summary>
        private static ExDrawingOutlineController instance;

        /// <summary>
        /// @yosef  29/12/19
        /// private constructor
        /// </summary>
        private ExDrawingOutlineController()
        {

        }

        /// <summary>
        /// @yosef  29/12/19
        /// get instance of the singletong
        /// </summary>
        public static ExDrawingOutlineController Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new ExDrawingOutlineController();
                }
                return instance;
            }
        }

        /// <summary>
        /// @yosef  29/12/19
        /// delegate of the MouseDown
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void ExOutlineHookMouseDownExt(object sender, MouseEventExtArgs e)
        {
            ExDrawingOutlineFSM dols_instance = ExDrawingOutlineFSM.Instance;

            if (dols_instance.getCurrentState() == ExDrawingOutlineFSM.ExDrawingOutlineState.DOLS_PREFERED)
            {
                System.Drawing.Point relatedPos = ExSlidePane.Instance.getPositionInPane();
                ExDrawingOutline.Instance.Left = relatedPos.X;
                ExDrawingOutline.Instance.Top = relatedPos.Y;
            }

            dols_instance.EmitInput(ExDrawingOutlineFSM.ExDrawingOutlineInput.DOLI_LBTNDOWN);

            ExDrawing.Instance.refresh();
        }

        /// <summary>
        /// @yosef  29/12/19
        /// delegate of the MouseMove
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void ExOutlineHookMouseMoveExt(object sender, MouseEventExtArgs e)
        {
            ExDrawingOutlineFSM dols_instance = ExDrawingOutlineFSM.Instance;

            if (ExSlidePane.Instance.Contains(e))
            {
                dols_instance.EmitInput(ExDrawingOutlineFSM.ExDrawingOutlineInput.DOLI_MOVEINPANE);
            }
            else
            {
                dols_instance.EmitInput(ExDrawingOutlineFSM.ExDrawingOutlineInput.DOLI_MOVEOUTPANE);
            }

            if (dols_instance.getCurrentState() == ExDrawingOutlineFSM.ExDrawingOutlineState.DOLS_DRAW)
            {
                System.Drawing.Point relatedPos = ExSlidePane.Instance.getPositionInPane();
                ExDrawingOutline.Instance.Width = relatedPos.X - ExDrawingOutline.Instance.Left;
                ExDrawingOutline.Instance.Height = relatedPos.Y - ExDrawingOutline.Instance.Top;
            }

            ExDrawing.Instance.refresh();
        }

        /// <summary>
        /// @yosef  29/12/19
        /// delegate of the MouseUp        
        /// @yosef  3/1/20  revised
        /// switch globalmousehook
        /// add new visualgrid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void ExOutlineHookMouseUpExt(object sender, MouseEventExtArgs e)
        {
            ExDrawingOutlineFSM dols_instance = ExDrawingOutlineFSM.Instance;
            
            if (dols_instance.getCurrentState() == ExDrawingOutlineFSM.ExDrawingOutlineState.DOLS_DRAW)
            {
                // it seems to check the mouse position is within the slidepane
                //ExGlobalFSMController.Instance.SwitchTo(ExGlobalFSM.GLOBAL_FSM_STATE.)
                ExGlobalMouseHookController.Instance.SwitchHook(ExGlobalMouseHookController.HOOK_STAT.HOOK_VG);
                ExVisualGridManager.Instance.addNewVisualGrid();
            }

            dols_instance.EmitInput(ExDrawingOutlineFSM.ExDrawingOutlineInput.DOLI_LBTNUP);

            ExDrawing.Instance.refresh();
        }
    }
}
