﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using PowerPoint = Microsoft.Office.Interop.PowerPoint;
using Office = Microsoft.Office.Core;
using System.IO;
using System.Diagnostics;
using System.Windows.Forms;
using Gma.System.MouseKeyHook;
using ExLide.Controllers;
using Microsoft.Office.Interop.PowerPoint;
using ExLide.Model.PowerPoint;
using ExLide.Services;
using ExLide.Views;
using ExLide.Model.FSM;
using ExLide.Views.Outline;

namespace ExLide
{

    public partial class ThisAddIn
    {

        private string AppDataFolder =
            Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "ExLide");
        private const string AppLogName = "ExLide.log";

        #region Helper Functions
        
        private void SetupLogger()
        {
            // check if folder exists and if not, create it
            if (!Directory.Exists(AppDataFolder))
            {
                Directory.CreateDirectory(AppDataFolder);
            }

            string fileName = DateTime.Now.ToString("yyyy-mm-dd") + AppLogName;
            string logPath = Path.Combine(AppDataFolder, fileName);

            Trace.AutoFlush = true;
            Trace.Listeners.Add(new TextWriterTraceListener(logPath));
        }

        #endregion

        #region Powerpoint Application Handlers

        private void ThisAddIn_Startup(object sender, System.EventArgs e)
        {
            ExGlobalMouseHookController.Instance.Init();
            new CustomContextMenu();
            PPMouseController.Init(Application);
            Application.AfterPresentationOpen += new EApplication_AfterPresentationOpenEventHandler(AfterPresentaOpenHanler);
            Application.PresentationBeforeClose += new EApplication_PresentationBeforeCloseEventHandler(PresentationBeforeCloseHandler);
            Application.AfterShapeSizeChange += new EApplication_AfterShapeSizeChangeEventHandler(AfterShapeSizeChangeHandler);
            Application.PresentationBeforeSave += new EApplication_PresentationBeforeSaveEventHandler(BeforeSavePresentationHandler);
            Application.SlideSelectionChanged += new EApplication_SlideSelectionChangedEventHandler(ApplicationSlideSelectionChanged);
            Application.SlideSelectionChanged += ExSelectedSlide.Instance.SetSlideRef;
        }
        /// <summary>
        /// This method is called Just after the presentation is opended. 
        /// We recover exlide metainfo stored in the presentation.
        /// </summary>
        /// <param name="presentation"></param>
        private void AfterPresentaOpenHanler(Presentation presentation)
        {
            DocumentMetaDataService.Instance.LoadExlideDocumentMetaData(presentation);
        }

        private void BeforeSavePresentationHandler(Presentation presentation, ref bool cancel) {
            DocumentMetaDataService.Instance.SaveExlideDocumentMetaData();
        }

        /// <summary>
        /// Callback to handle Shape resize. Used to detect changes in Grids
        /// </summary>
        /// <param name="shape"></param>
        private void AfterShapeSizeChangeHandler(Shape shape)
        {
            GridService.Instance.GridSizeChaged(shape);
        }

        private void PresentationBeforeCloseHandler(Presentation presentation, ref bool cancel)
        {
            DocumentMetaDataService.Instance.RemovePresentation(presentation);
        }

        private void ApplicationSlideSelectionChanged(SlideRange sldRange)
        {
            Presentation currentPresentation = Globals.ThisAddIn.Application.ActivePresentation;
            /*
            float slideHeight = currentPresentation.PageSetup.SlideHeight;
            float slideWidth = currentPresentation.PageSetup.SlideWidth;
            float swidth = currentPresentation.SlideMaster.Height;
            float sheight =currentPresentation.SlideMaster.Width;

            DocumentWindows documentWindows = currentPresentation.Windows;
            DocumentWindow documentWindow = documentWindows[1];
            PowerPoint.View view = documentWindow.View;
            int zoom = view.Zoom;

            PowerPoint.Panes panes = documentWindow.Panes;
            PowerPoint.Pane pane = panes[2];

            int splitHorizontal = documentWindow.SplitHorizontal;
            int splitVertical = documentWindow.SplitVertical;

            int splitHor = documentWindow.SplitHorizontal;
            int splitVer = documentWindow.SplitVertical;
            */
        }

        private void ThisAddIn_Shutdown(object sender, System.EventArgs e)
        {
        }

        /// <summary>
        /// Callback to cancel the grid creation.
        /// </summary>
        /// <param name="Sel"></param>
        /// <param name="Cancel"></param>

        #endregion

        #region VSTO generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InternalStartup()
        {
            this.Startup += new System.EventHandler(ThisAddIn_Startup);
            this.Shutdown += new System.EventHandler(ThisAddIn_Shutdown);
        }
        
#endregion
    }
}
