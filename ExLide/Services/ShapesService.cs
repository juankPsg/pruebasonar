﻿using Microsoft.Office.Core;
using Microsoft.Office.Interop.PowerPoint;
using System;
using System.Drawing;
using Shape = Microsoft.Office.Interop.PowerPoint.Shape;
using ShapeRange = Microsoft.Office.Interop.PowerPoint.ShapeRange;

namespace ExLide.Services
{
    /// <summary>
    /// This service is a Wrapper of powerPoint shapes
    /// </summary>
    public sealed class ShapesService
    {
        PowerPointService powerPointSrv;
        #region define Singleton
        /// <summary> instance of this Singleton Object </summary>
        private static ShapesService instance = null;
        private static readonly object padlock = new object();

        /// <summary> constructor </summary>
        ShapesService()
        {
            powerPointSrv = PowerPointService.Instance;
        }

        /// <summary> Instance of the PowerPointService </summary>
        public static ShapesService Instance
        {
            get
            {
                lock (padlock)
                {
                    if (instance == null)
                    {
                        instance = new ShapesService();
                    }
                    return instance;
                }
            }
        }
        #endregion

        #region define methods
        /// <summary> 
        /// create a new rectangle shape object to add into slide
        /// </summary>
        public Shape NewRectangle(Slide targetSlide, float left, float top, float width, float height)
        {
            Shape rectangle = targetSlide.Shapes.AddShape(
                            MsoAutoShapeType.msoShapeRectangle, left, top, width, height);

            rectangle.Name = BuildExlideId(rectangle.Type.ToString(), rectangle.Id.ToString());
            rectangle.ZOrder(MsoZOrderCmd.msoSendToBack);
            return rectangle;
        }
        /// <summary> 
        /// create a new textbox shape object to add into slide
        /// </summary>
        public Shape NewTextBox(float left, float top, float width, float height)
        {
            Slide targetSlide = powerPointSrv.getCurrentSlide();
            Shape textBox = targetSlide.Shapes.AddTextbox(
                MsoTextOrientation.msoTextOrientationHorizontal, left, top, width, height);
            textBox.Name = BuildExlideId(textBox.Type.ToString(), textBox.Id.ToString());
            decorateShape(textBox);

            return textBox;
        }


        public string BuildExlideId(string type, string shapeId)
        {
            return "Ex_" + type + "_" + shapeId;
        }
        /// <summary>
        /// Add black line to an alement and change it`s backgound color to white color
        /// </summary>
        /// <param name="component"></param>
        private void decorateShape(Shape component)
        {
            component.Line.DashStyle = MsoLineDashStyle.msoLineSolid;
            component.Line.ForeColor.RGB = Color.Black.ToArgb();
            component.Fill.ForeColor.RGB = Color.White.ToArgb();
        }
        /// <summary>
        /// Get the selected shape into a slide
        /// </summary>
        /// <returns></returns>
        public ShapeRange GetSelectedShape()
        {
            try { 
                return powerPointSrv.GetCurrentDocument().Selection.ShapeRange;
            }
            catch(Exception e)
            {
                return null;
            }
            
        }

        #endregion
    }
}
