﻿using ExLide.Controllers;
using ExLide.Model.DataLayer;
using ExLide.Model.Outline;
using ExLide.Model.PowerPoint;
using ExLide.Static.MouseKeyHook;
using Gma.System.MouseKeyHook;
using Microsoft.Office.Core;
using Microsoft.Office.Interop.PowerPoint;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Point = System.Drawing.Point;
using Shape = Microsoft.Office.Interop.PowerPoint.Shape;

namespace ExLide.Services
{
    public sealed class GridService
    {
        ShapesService ShapesSrv;
        DocumentMetaDataService ExMetadataSrv;
        IKeyboardMouseEvents m_GlobalHook = null;
        // Create the Keyboard Hook
        KeyboardHook keyboardHook = null;
        GridData currentGrid;

        #region define Singleton
        /// <summary> instance of this Singleton Object </summary>
        private static GridService instance = null;
        private static readonly object padlock = new object();

        /// <summary> constructor </summary>
        GridService()
        {
            ExMetadataSrv = DocumentMetaDataService.Instance;
            ShapesSrv = ShapesService.Instance;
        }

        /// <summary> Instance of the GridService </summary>
        public static GridService Instance
        {
            get
            {
                lock (padlock)
                {
                    if (instance == null)
                    {
                        instance = new GridService();
                    }
                    return instance;
                }
            }
        }
        #endregion

        #region Draw a new grid
        /// <summary> create a new grid with current configuration </summary>
        public void newGrid(Slide targetSlide, GridData grid)
        {
            //Validaciones
            if (null == targetSlide || !grid.isValid())
            {
                //thow exception
                return;
            }


            float leftPosition = grid.LeftPosition;
            float topPosition = grid.TopPosition;
            List<string> cellNames = new List<string>();

            for (int row = 1; row <= grid.Rows; row++)
            {
                for (int column = 1; column <= grid.Cols; column++)
                {
                    Shape rectangle = ShapesSrv.NewRectangle(targetSlide, leftPosition, topPosition, grid.CellWidth, grid.CellHeight);
                    cellNames.Add(rectangle.Name);
                    leftPosition += grid.CellWidth;
                }
                leftPosition = grid.LeftPosition;
                topPosition += grid.CellHeight;
            }
            Shape gridGroup = targetSlide.Shapes.Range(cellNames.ToArray()).Group();
            grid.Id = gridGroup.Name = ShapesSrv.BuildExlideId("VisualGrid", gridGroup.Id.ToString());
            //TODO: Publish the creation of a Grid in an Event to decople this call 
            ExMetadataSrv.AddGridData(grid);
            Cursor.Current = Cursors.WaitCursor;
        }

        public float CalculateTopCellPosition(int columns, float height, float top, int gridPosition)
        {
            return Convert.ToInt16(((height / columns) + top) * (gridPosition - 1));
        }
        public float CalculateLeftCellPosition(int rows, float width, float left, int gridPosition)
        {
            return Convert.ToInt16(((width / rows) + left) * (gridPosition - 1));
        }
        /// <summary>
        /// Group the created component to the selected grid.
        /// </summary>
        /// <param name="targetSlide"></param>
        /// <param name="componentsNameToGruop"></param>
        public bool GroupGrid(Slide targetSlide, List<string> componentsNameToGruop)
        {
            try
            {
                Shape gridGroup = targetSlide.Shapes.Range(componentsNameToGruop.ToArray()).Group();
                // componentsNameToGruop[0] == the name of the current grid
                gridGroup.Name = componentsNameToGruop[0];
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public void GridSizeChaged(Shape grid)
        {
            //TODO Add actions to execute into the current method
            return;
        }

        public void NewGridUsingMouse(GridData grid)
        {
            currentGrid = grid;
            m_GlobalHook = Hook.GlobalEvents();
            keyboardHook = new KeyboardHook();

            m_GlobalHook.MouseDownExt += MouseDownExt;
            m_GlobalHook.MouseUpExt += MouseUpExt;
            m_GlobalHook.MouseMoveExt += MouseMoveExt;
            keyboardHook.KeyUp += new KeyboardHook.KeyboardHookCallback(keyboardHook_KeyUp);
            //Installing the Keyboard Hooks
            keyboardHook.Install();
        }

        private void MouseMoveExt(object sender, MouseEventExtArgs e)
        {
            Cursor.Current = Cursors.Cross;
        }

        private void keyboardHook_KeyUp(KeyboardHook.VKeys key)
        {
            if (key.ToString() == "ESCAPE")
                UnListenerEvents();
        }

        private void MouseDownExt(object sender, MouseEventArgs e)
        {
            switch (e.Button)
            {
                case MouseButtons.Left:
                    Point finalPosition = ExSlidePane.Instance.getPositionInPane();
                    //Store the positions where the mouse was clicked
                    currentGrid.TopPosition = finalPosition.Y;
                    currentGrid.LeftPosition = finalPosition.X;
                    break;
                case MouseButtons.None:
                    break;
                case MouseButtons.Right:
                    break;
                case MouseButtons.Middle:
                    break;
                case MouseButtons.XButton1:
                    break;
                case MouseButtons.XButton2:
                    break;
            }
        }

        /// <summary>
        /// This method obtain the last pointer position into the slide.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MouseUpExt(object sender, MouseEventArgs e)
        {
            Point finalPosition = ExSlidePane.Instance.getPositionInPane();

            currentGrid.Height = Math.Abs(finalPosition.Y - currentGrid.TopPosition);
            currentGrid.Width = Math.Abs(finalPosition.X- currentGrid.LeftPosition);

            if (0 == currentGrid.Height)
            {
                //set the default Height value
                currentGrid.Height = PowerPointService.Instance.getHeight() - currentGrid.TopPosition;
            }
            if (0 == currentGrid.Width)
            {
                //set the default Width value
                currentGrid.Width = PowerPointService.Instance.getWidth() - currentGrid.LeftPosition;
            }

            switch (e.Button)
            {
                case MouseButtons.Left:
                   // if (finalPosition.X >= 0 && finalPosition.Y >= 0)
                        newGrid(PowerPointService.Instance.getCurrentSlide(), currentGrid);
                    break;
                case MouseButtons.None:
                    break;
                case MouseButtons.Right:
                    break;
                case MouseButtons.Middle:
                    break;
                case MouseButtons.XButton1:
                    break;
                case MouseButtons.XButton2:
                    break;
            }
            UnListenerEvents();
        }

        private void UnListenerEvents()
        {
            m_GlobalHook.MouseDownExt -= MouseDownExt;
            m_GlobalHook.MouseUpExt -= MouseUpExt;
            m_GlobalHook.MouseMoveExt -= MouseMoveExt;
            keyboardHook.KeyUp -= new KeyboardHook.KeyboardHookCallback(keyboardHook_KeyUp);
            keyboardHook.Uninstall();

            m_GlobalHook.Dispose();
            Cursor.Current = Cursors.Default;
        }
        #endregion
    }
}
