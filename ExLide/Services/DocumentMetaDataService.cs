﻿using ExLide.Model.DataLayer;
using Microsoft.Office.Interop.PowerPoint;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Shapes;
using Shape = Microsoft.Office.Interop.PowerPoint.Shape;

namespace ExLide.Services
{
    public sealed class DocumentMetaDataService
    {
        #region Attributes
        /// <summary>
        /// documentGrids contains the document's GridData objects
        /// </summary>
        private List<GridData> documentGrids = new List<GridData>();
        private List<PresentationData> openPresentationsData = new List<PresentationData>();

        private PowerPointService powerPointSrv;
        
        public static string ExMetaDataTagName = "EXMETADATA";
        #endregion

        #region define Singleton
        /// <summary> instance of this Singleton Object </summary>
        private static DocumentMetaDataService instance = null;
        private static readonly object padlock = new object();

        /// <summary> constructor </summary>
        DocumentMetaDataService()
        {
            powerPointSrv = PowerPointService.Instance;
        }

        /// <summary> Instance of the PowerPointService </summary>
        public static DocumentMetaDataService Instance
        {
            get
            {
                lock (padlock)
                {
                    if (instance == null)
                    {
                        instance = new DocumentMetaDataService();
                    }
                    return instance;
                }
            }
        }
        #endregion

        #region Methods

        /// <summary>
        /// create json object with grid information
        /// </summary>
        /// <param name="gridData"></param>
        public void AddGridData(GridData grid)
        {
            PresentationData presentationData = getCurrentPresentationData();
            if(null != presentationData) { 
                presentationData.AddGrid(grid);
            }
        }

        /// <summary>
        /// Save json created into powerpoint tag metadata
        /// </summary>
        public void SaveExlideDocumentMetaData()
        {
            if (openPresentationsData.Count <= 0)
            {
                return;
            }

            Presentation presentation = powerPointSrv.getCurrentPresentation();
            PresentationData presentationData = getCurrentPresentationData();

            if (null == presentationData)
            {
                return;
            }

            if (presentationData.Id == presentation.Name)
            {
                JObject ExlideDocumentData = presentationData.ToJSON();
                string jsonToOutput = JsonConvert.SerializeObject(ExlideDocumentData, Formatting.Indented);
                presentation.Tags.Add(ExMetaDataTagName, jsonToOutput);
            }

        }

        /// <summary>
        /// Load the json object saved into metadata
        /// </summary>
        /// <param name="jsonTagName"></param>
        /// <returns></returns>
        private string LoadDocumentMetaDataByTagName(string tagName)
        {
            Presentation presentation = powerPointSrv.getCurrentPresentation();
            int numTags = presentation.Tags.Count;
            for (int tagPosition = 1; tagPosition <= numTags; tagPosition++)
                if (presentation.Tags.Name(tagPosition) == tagName)
                {
                    return presentation.Tags.Value(tagPosition);
                }
            return null;
        }

        /// <summary>
        /// Load the Exlide Metadata stored in the presentation.
        /// </summary>
        public void LoadExlideDocumentMetaData(Presentation presentation)
        {
            string config = LoadDocumentMetaDataByTagName(ExMetaDataTagName);
            PresentationData presentationData = new PresentationData();
            presentationData.Id = presentation.Name;
            //TODO Recuperar datos en objetos de la clase
            JObject metaData = JObject.Parse(config);
            presentationData.ImportFromJObject(metaData);
            openPresentationsData.Add(presentationData);
            
        }

        /// <summary>
        /// Returns a GridData object by Id if this Id is found in the grid collection of the presentation
        /// </summary>
        /// <param name="gridId"></param>
        /// <returns></returns>
        public GridData GetGridDataById(string gridId)
        {
            PresentationData presentationData = getCurrentPresentationData();

            if (null == presentationData)
            {
                return null;
            }
            GridData gridData = presentationData.GetGridDataById(gridId);
            return gridData;
        }

        public PresentationData getCurrentPresentationData()
        {
            Presentation presentation = powerPointSrv.getCurrentPresentation();
            foreach (PresentationData presentationData in openPresentationsData)
            {
                if (presentationData.Id == presentation.Name)
                {
                    return presentationData;
                }
            }
            return null;
        }

        #endregion

        public void RemovePresentation(Presentation presentation)
        {
            PresentationData currentPresentation = getCurrentPresentationData();
            if(null != currentPresentation) {
                openPresentationsData.Remove(currentPresentation);
            }
        }
    }
}
