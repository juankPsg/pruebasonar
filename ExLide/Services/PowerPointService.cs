﻿using Microsoft.Office.Interop.PowerPoint;
using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using exlideGlobals = ExLide.Globals;
using System.Runtime.InteropServices.WindowsRuntime;

namespace ExLide.Services
{
    public sealed class PowerPointService
    {

        #region define Singleton
        /// <summary> instance of this Singleton Object </summary>
        private static PowerPointService instance = null;
        private static readonly object padlock = new object();

        /// <summary> constructor </summary>
        PowerPointService()
        {

        }

        /// <summary> Instance of the PowerPointService </summary>
        public static PowerPointService Instance
        {
            get
            {
                lock (padlock)
                {
                    if (instance == null)
                    {
                        instance = new PowerPointService();
                        instance.init();
                    }
                    return instance;
                }
            }
        }
        #endregion

        #region PowePoint configuration
        /// <summary> 
        /// Obtain the powerpoint current active slide
        /// </summary>
        public Slide getCurrentSlide()
        {
            if (!isSlideSelected())
            {
                return null;
            }
            return exlideGlobals.ThisAddIn.Application.ActivePresentation.Slides[exlideGlobals.ThisAddIn.Application.ActiveWindow.Selection.SlideRange.SlideIndex];
        }

        /// <summary>
        /// Detect if there is an slide selected
        /// </summary>
        /// <returns></returns>
        public bool isSlideSelected()
        {
            try
            {
                int slideRangeCount = exlideGlobals.ThisAddIn.Application.ActiveWindow.Selection.SlideRange.Count;
                if(slideRangeCount  > 0) { 
                    return true; 
                }
                return false;
            }catch(Exception e)
            {
                return false;
            }
        }

        public Presentation getCurrentPresentation()
        {
            return exlideGlobals.ThisAddIn.Application.ActivePresentation;
        }

        public bool IsActivePresentation()
        {
            try
            {
                if (null != exlideGlobals.ThisAddIn.Application.ActivePresentation)
                {
                    return true;
                }
                return false;
            }
            catch(Exception e)
            {
                return false;
            }
            
        }

        public DocumentWindow GetCurrentDocument()
        {
            return exlideGlobals.ThisAddIn.Application.ActiveWindow;
        }

        private void init()
        {
            //ppHandle = Process.GetCurrentProcess().MainWindowHandle;
            // ppSlideView = ExVersionCompatible.FindSlideViewHandle(ppHandle);
            Presentation CurrentPresentation = getCurrentPresentation();
            slideWidth = CurrentPresentation.PageSetup.SlideWidth;
            slideHeight = CurrentPresentation.PageSetup.SlideHeight;
        }

        public float getWidth()
        {
            return slideWidth;
        }

        public float getHeight()
        {
            return slideHeight;
        }
        #endregion

        #region define member vars
        /// <summary>
        /// @yosef  29/12/19
        /// handle of the powerpoint process
        /// </summary>
        private IntPtr ppHandle = IntPtr.Zero;

        /// <summary>
        /// @yosef  29/12/19
        /// handle of the slideview
        /// in normal view, it consistsof 1) SlidePane 2) NotesPane 3)OutlinePane
        /// </summary>
        public IntPtr ppSlideView { get; } = IntPtr.Zero;

        /// <summary>
        /// @yosef  29/12/19
        /// instance of the presenation of the current process
        /// </summary>
        public Presentation CurrentPresentation { get; }

        /// <summary>
        /// @yosef  29/12/19
        /// SlideWidth of PageSetup in Presentation
        /// this field might be defined in the different Class
        /// </summary>
        private float slideWidth = 0;

        /// <summary>
        /// @yosef  29/12/19
        /// SlideHeight of PageSetup in Presentation
        /// this field might be defined in the different Class
        /// </summary>
        private float slideHeight = 0;

        /// <summary>
        /// @yosef  3/1/20
        /// View Width of the Slide
        /// this field must be fixed to get dynamically in the future
        /// </summary>
        public float SlideViewWidth { get; } = 1280;

        /// <summary>
        /// @yosef  3/1/20
        /// View Height of the Slide
        /// this field must be fixed to get dynamically in the future
        /// </summary>
        public float SlideViewHeight { get; } = 720;
        #endregion
    }
}