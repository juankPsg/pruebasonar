﻿using ExLide.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExLide.Components
{
    public abstract class ExAbstractBaseComponent : IExComponent
    {
        ShapesService shapeSrv;
        string Name { get; }

        public ExAbstractBaseComponent()
        {
            shapeSrv = ShapesService.Instance;
        }

        public abstract void Create();
    }
}
