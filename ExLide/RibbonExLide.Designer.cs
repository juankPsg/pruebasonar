﻿namespace ExLide
{
    partial class RibbonExLide : Microsoft.Office.Tools.Ribbon.RibbonBase
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        public RibbonExLide()
            : base(Globals.Factory.GetRibbonFactory())
        {
            InitializeComponent();
        }

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RibbonExLide));
            this.tab_ExLide = this.Factory.CreateRibbonTab();
            this.group1 = this.Factory.CreateRibbonGroup();
            this.ins_vg = this.Factory.CreateRibbonButton();
            this.newComponent = this.Factory.CreateRibbonButton();
            this.button3 = this.Factory.CreateRibbonButton();
            this.group2 = this.Factory.CreateRibbonGroup();
            this.group3 = this.Factory.CreateRibbonGroup();
            this.group4 = this.Factory.CreateRibbonGroup();
            this.splitButton1 = this.Factory.CreateRibbonSplitButton();
            this.group5 = this.Factory.CreateRibbonGroup();
            this.splitButton2 = this.Factory.CreateRibbonSplitButton();
            this.button4 = this.Factory.CreateRibbonButton();
            this.tab_ExLide.SuspendLayout();
            this.group1.SuspendLayout();
            this.group4.SuspendLayout();
            this.group5.SuspendLayout();
            this.SuspendLayout();
            // 
            // tab_ExLide
            // 
            this.tab_ExLide.Groups.Add(this.group1);
            this.tab_ExLide.Groups.Add(this.group2);
            this.tab_ExLide.Groups.Add(this.group3);
            this.tab_ExLide.Groups.Add(this.group4);
            this.tab_ExLide.Groups.Add(this.group5);
            this.tab_ExLide.Label = "ExLide";
            this.tab_ExLide.Name = "tab_ExLide";
            // 
            // group1
            // 
            this.group1.Items.Add(this.ins_vg);
            this.group1.Items.Add(this.newComponent);
            this.group1.Items.Add(this.button3);
            this.group1.Label = "Exlide Grid";
            this.group1.Name = "group1";
            // 
            // ins_vg
            // 
            this.ins_vg.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge;
            this.ins_vg.Image = ((System.Drawing.Image)(resources.GetObject("ins_vg.Image")));
            this.ins_vg.Label = "New";
            this.ins_vg.Name = "ins_vg";
            this.ins_vg.ShowImage = true;
            this.ins_vg.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.NewExlideGridClick);
            // 
            // newComponent
            // 
            this.newComponent.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge;
            this.newComponent.Image = ((System.Drawing.Image)(resources.GetObject("newComponent.Image")));
            this.newComponent.Label = "Add Component";
            this.newComponent.Name = "newComponent";
            this.newComponent.ShowImage = true;
            this.newComponent.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.NewComponentClick);
            // 
            // button3
            // 
            this.button3.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge;
            this.button3.Image = ((System.Drawing.Image)(resources.GetObject("button3.Image")));
            this.button3.Label = "Save as Template";
            this.button3.Name = "button3";
            this.button3.ShowImage = true;
            // 
            // group2
            // 
            this.group2.Label = "Format";
            this.group2.Name = "group2";
            // 
            // group3
            // 
            this.group3.Label = "Tools";
            this.group3.Name = "group3";
            // 
            // group4
            // 
            this.group4.Items.Add(this.splitButton1);
            this.group4.Label = "Share";
            this.group4.Name = "group4";
            // 
            // splitButton1
            // 
            this.splitButton1.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge;
            this.splitButton1.Image = ((System.Drawing.Image)(resources.GetObject("splitButton1.Image")));
            this.splitButton1.Label = "Send";
            this.splitButton1.Name = "splitButton1";
            // 
            // group5
            // 
            this.group5.Items.Add(this.splitButton2);
            this.group5.Items.Add(this.button4);
            this.group5.Label = "About";
            this.group5.Name = "group5";
            // 
            // splitButton2
            // 
            this.splitButton2.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge;
            this.splitButton2.Image = ((System.Drawing.Image)(resources.GetObject("splitButton2.Image")));
            this.splitButton2.Label = "Feedback";
            this.splitButton2.Name = "splitButton2";
            // 
            // button4
            // 
            this.button4.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge;
            this.button4.Image = ((System.Drawing.Image)(resources.GetObject("button4.Image")));
            this.button4.Label = "Setting";
            this.button4.Name = "button4";
            this.button4.ShowImage = true;
            // 
            // RibbonExLide
            // 
            this.Name = "RibbonExLide";
            this.RibbonType = "Microsoft.PowerPoint.Presentation";
            this.Tabs.Add(this.tab_ExLide);
            this.Load += new Microsoft.Office.Tools.Ribbon.RibbonUIEventHandler(this.RibbonExLide_Load);
            this.tab_ExLide.ResumeLayout(false);
            this.tab_ExLide.PerformLayout();
            this.group1.ResumeLayout(false);
            this.group1.PerformLayout();
            this.group4.ResumeLayout(false);
            this.group4.PerformLayout();
            this.group5.ResumeLayout(false);
            this.group5.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private Microsoft.Office.Tools.Ribbon.RibbonTab tab_ExLide;
        internal Microsoft.Office.Tools.Ribbon.RibbonGroup group1;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton newComponent;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton ins_vg;
        internal Microsoft.Office.Tools.Ribbon.RibbonGroup group2;
        internal Microsoft.Office.Tools.Ribbon.RibbonGroup group3;
        internal Microsoft.Office.Tools.Ribbon.RibbonGroup group4;
        internal Microsoft.Office.Tools.Ribbon.RibbonGroup group5;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton button3;
        internal Microsoft.Office.Tools.Ribbon.RibbonSplitButton splitButton1;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton button4;
        internal Microsoft.Office.Tools.Ribbon.RibbonSplitButton splitButton2;

        public string GetCustomUI(string RibbonID)
        {
            throw new System.NotImplementedException();
        }
    }

    partial class ThisRibbonCollection
    {
        internal RibbonExLide RibbonExLide
        {
            get { return this.GetRibbon<RibbonExLide>(); }
        }
    }
}
