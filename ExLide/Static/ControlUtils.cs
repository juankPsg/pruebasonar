﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace ExLide.Static
{
    public class ControlUtils
    {
        public static bool BorderContains(MouseEventArgs e, Border border)
        {
            Point relatedPos = e.GetPosition(border);
            if (relatedPos.X >= 0 && relatedPos.X <= border.Width &&
                relatedPos.Y >= 0 && relatedPos.Y <= border.Height)
            {
                return true;
            }
            return false;
        }

        public static bool GridContains(MouseEventArgs e, Grid grid)
        {
            Point relatedPos = e.GetPosition(grid);
            if (relatedPos.X >= 0 && relatedPos.X <= grid.ActualWidth &&
                relatedPos.Y >= 0 && relatedPos.Y <= grid.ActualHeight)
            {
                return true;
            }
            return false;
        }
    }
}
