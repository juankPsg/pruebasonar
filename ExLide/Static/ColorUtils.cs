﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace ExLide.Static
{
    public class ColorUtils
    {
        public static Color getColor(string colorString)
        {
            return (Color)ColorConverter.ConvertFromString(colorString);
        }
    }
}
