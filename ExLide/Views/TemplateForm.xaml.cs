﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ExLide.Controllers;
using ExLide.Static;

namespace ExLide.Views
{
    /// <summary>
    /// Interaction logic for TemplateForm.xaml
    /// </summary>
    public partial class TemplateForm : Window
    {
        private bool isMoving = false;
        private Point moveStartPos;
        private Point moveStartWindowPos;
        private int selectedtemplate = -1;
        VisualGridForm gridForm;

        public TemplateForm()
        {
            InitializeComponent();
            loadTemplates();

            btn_cancelTemplate.Click += CancelButtonHandler;
            btn_insertTemplate.Click += OpenGridFormWindow;

            thumbnailView.PreviewMouseDown += OnPreviewMouseDown;
            thumbnailView.PreviewMouseUp += OnPreviewMouseUp;
            gridForm = null;
        }

        ///<sumary>
        ///Handle the template selection
        ///</sumary>
        private void loadTemplates()
        {
        }

        private void OnPreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            if (ControlUtils.BorderContains(e, tpl_ass))
            {
                selectedtemplate = 0;
            }
        }

        ///<sumary>
        /// Hide the current windows and open the preview windows template selected
        ///</sumary>
        private void OnPreviewMouseUp(Object sender, MouseButtonEventArgs e)
        {
            if (selectedtemplate == 0 && ControlUtils.BorderContains(e, tpl_ass))
            {
                thumbnailView.Visibility = Visibility.Hidden;
                detailView.Visibility = Visibility.Visible;
            }
            selectedtemplate = -1;
        }

        ///<sumary>
        /// Hide the current windows and open the template windows
        ///</sumary>
        private void CancelButtonHandler(object sender, RoutedEventArgs e)
        {
            detailView.Visibility = Visibility.Hidden;
            thumbnailView.Visibility = Visibility.Visible;
        }

        ///<sumary>
        /// Get the current template selected by user
        ///</sumary>
        private void templateSelected(int selectedtemplate)
        {
            GridController.instance.templateSelected(selectedtemplate);
        }

        ///<sumary>
        /// Opens the grid form selector
        ///</sumary>
        private void OpenGridFormWindow(object sendcer, RoutedEventArgs e)
        {
            //TODO get values from selected template
            templateSelected(0);
            //Show the grid dimention selector window
            gridForm = new VisualGridForm();
            gridForm.Show();
            Close();
        }

        protected override void OnMouseLeftButtonDown(MouseButtonEventArgs e)
        {
            if (ControlUtils.GridContains(e, title))
            {
                isMoving = true;

                Point relatedPos = e.GetPosition(this);
                moveStartPos = this.PointToScreen(relatedPos);
                moveStartWindowPos.X = this.Left;
                moveStartWindowPos.Y = this.Top;
            }

            base.OnMouseDown(e);
        }

        protected override void OnMouseMove(MouseEventArgs e)
        {
            if (isMoving)
            {
                Point movedPos = this.PointToScreen(e.GetPosition(this));
                Vector delta = movedPos - moveStartPos;
                this.Left = moveStartWindowPos.X + delta.X;
                this.Top = moveStartWindowPos.Y + delta.Y;
                return;
            }

            if (ControlUtils.BorderContains(e, tpl_ass))
            {
                tpl_ass.BorderThickness = new Thickness(1);
            }
            else
            {
                tpl_ass.BorderThickness = new Thickness(0);
            }

            base.OnMouseMove(e);
        }

        protected override void OnMouseUp(MouseButtonEventArgs e)
        {
            selectedtemplate = -1;
            if (isMoving)
            {
                isMoving = false;
            }
        }
        private void WindowClosed(object sender, EventArgs e)
        {
            Close();
        }
    }
}
