﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ExLide.Model.FSM;
using ExLide.Views.Outline;

namespace ExLide.Views
{
    /// <summary>
    /// @yosef  29/12/19
    /// Abstraction of drawing in ExLide
    /// </summary>
    public class ExDrawing
    {
        #region Singleton Pattern

        /// <summary>
        /// @yosef  29/12/19
        /// instance of ExDrawing
        /// </summary>
        private static ExDrawing instance = null;

        /// <summary>
        /// @yosef  29/12/19
        /// private constructor
        /// </summary>
        private ExDrawing()
        {

        }

        /// <summary>
        /// @yosef  29/12/19
        /// </summary>
        public static ExDrawing Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new ExDrawing();
                }
                return instance;
            }
        }

        #endregion

        public void refresh()
        {
            switch (ExGlobalFSM.Instance.currentState)
            {
                case ExGlobalFSM.GLOBAL_FSM_STATE.GFS_OUTLINE:
                    Ex2DrawingOutline.Instance.refresh();
                    break;
            }
        }
    }
}
