﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using ExLide.Controllers;
using ExLide.Model;
using ExLide.Model.VisualGrid;
using ExLide.Services;
using ExLide.Static;
using ExLide.Views.Outline;
using Microsoft.Office.Interop.PowerPoint;
using Point = System.Windows.Point;

namespace ExLide.Views
{
    /// <summary>
    /// @yosef   3/1/20  marked
    /// Interaction logic for VisualGrid.xaml
    /// </summary>
    public partial class VisualGridForm : Window
    {
        /// <summary>
        /// @yosef   3/1/20  marked
        ///  flag used for indicating the visual grid window is moving or not
        ///  activated when the left-button click and none of the following actions occur
        ///  1) select visual grid type
        ///  2) select visual grid size
        ///  3) select import data from excel
        /// </summary>
        private bool isMoving = false;

        /// <summary>
        /// @yosef   3/1/20  marked
        /// when left button is clicked and when have to move window in this case
        /// the start position should be stored
        /// </summary>
        private Point moveStartPos;

        /// <summary>
        /// @yosef   3/1/20  marked
        /// when left button is clicked and when have to move window in this case
        /// the (Left, Top) position is stored
        /// </summary>
        private Point moveStartWindowPos;

        /// <summary>
        /// @yosef   3/1/20  marked
        /// size of rect in the right grid panel
        /// </summary>
        private Size gridRectSize;

        /// <summary>
        /// @yosef   3/1/20  marked
        /// the following 2 vars is used to highlight rect(s) in the grid
        /// </summary>
        private int prevSelRectRowNum = 0;
        private int prevSelRectColNum = 0;
        private bool selHoldOn = false;


        /// <summary>
        /// @yosef   3/1/20  marked
        /// the following(s) are highlighted items
        /// </summary>
        private TextBox[] rowNumText;
        private TextBox[] colNumText;
        private Rectangle[][] gridRect;

        TemplateForm templateForm;

        private List<ExVisualGridPreview> previewList;
        public List<ExVisualGridPreview> PreviewList
        {
            get
            {
                return previewList;
            }
        }

        /// <summary>
        /// Constructor
        /// </summary>
        public VisualGridForm()
        {
            InitializeComponent();

            this.DataContext = this; // for Data Binding
            CreateVGPreviewList();
            CreateVisualMatrixDimentionSelector();

            closeGridDimentions.Click += CancelGridCreation;
            rowsGridnumber.LostFocus += ShapeTextBoxLostFocus;
            columnsGridnumber.LostFocus += ShapeTextBoxLostFocus;
            templateForm = null;
        }

        /// <summary>
        /// @yosef   3/1/20  marked
        /// Create VisualGrid PreviewList in the LeftPanel
        /// </summary>
        private void CreateVGPreviewList()
        {
            // @yosef - 26/12/19
            // ToDo 1: Create PreviewList in the ListView

            previewList = ExVisualGridPreviewManager.Instance.getPreviewList();


            vgPreviewList.SelectedIndex = 0;
        }

        /// <summary>
        /// Create a rectangle matrix to be used as a visual dimention selector.
        /// </summary>
        private void CreateVisualMatrixDimentionSelector()
        {
            // @yosef - 26/12/19
            // ToDo 1 : add rectangle to vgSizeGrid
            // ToDo 1 - 1: add row and column definitions to the grid
            for (int i = 0; i < ExVisualGridMetaData.MAX_ROW; ++i)
            {
                vgSizeGrid.RowDefinitions.Add(new RowDefinition());
            }
            for (int j = 0; j < ExVisualGridMetaData.MAX_COL; ++j)
            {
                vgSizeGrid.ColumnDefinitions.Add(new ColumnDefinition());
            }

            // calculate the width & height of the rectangles
            //      contraint 1 : the width and height must be the same size
            double rect_width = Math.Min(vgSizeGrid.Width / ExVisualGridMetaData.MAX_ROW,
                vgSizeGrid.Height / ExVisualGridMetaData.MAX_COL) - ExVisualGridMetaData.GRID_RECT_MARGIN;
            double rect_height = rect_width;
            gridRectSize = new Size(rect_width, rect_height);

            // Add rectangles to the rowsOfRect
            gridRect = new Rectangle[ExVisualGridMetaData.MAX_ROW][];
            for (int i = 0; i < ExVisualGridMetaData.MAX_ROW; ++i)
            {
                gridRect[i] = new Rectangle[ExVisualGridMetaData.MAX_COL];
                for (int j = 0; j < ExVisualGridMetaData.MAX_COL; ++j)
                {
                    gridRect[i][j] = new Rectangle();
                    gridRect[i][j].Width = rect_width;
                    gridRect[i][j].Height = rect_height;
                    gridRect[i][j].StrokeThickness = ExVisualGridMetaData.RectGridFormat.StrokeThickness;
                    gridRect[i][j].Stroke = new SolidColorBrush(
                        (Color)ColorConverter.ConvertFromString(ExVisualGridMetaData.RectGridFormat.StrokeColorString));
                    Grid.SetRow(gridRect[i][j], i);
                    Grid.SetColumn(gridRect[i][j], j);

                    vgSizeGrid.Children.Add(gridRect[i][j]);
                }
            }

            //add row & col numbers if SHOW_RC_NUM == true
            if (ExVisualGridMetaData.SHOW_RC_NUM)
            {
                rowNumText = new TextBox[ExVisualGridMetaData.MAX_ROW];
                for (int i = 0; i < ExVisualGridMetaData.MAX_ROW; ++i)
                {
                    rowNumText[i] = NewTextBox(i);
                    Grid.SetRow(rowNumText[i], i);
                    vgSizeGrid.Children.Add(rowNumText[i]);
                }
                colNumText = new TextBox[ExVisualGridMetaData.MAX_COL];
                for (int j = 1; j < ExVisualGridMetaData.MAX_COL; ++j)
                {
                    colNumText[j] = NewTextBox(j);
                    Grid.SetColumn(colNumText[j], j);
                    vgSizeGrid.Children.Add(colNumText[j]);
                }
            }
        }

        /// <summary>
        /// Russian Code
        /// Create a new textbox object
        /// </summary>
        private TextBox NewTextBox(int index)
        {
            TextBox textBox = new TextBox
            {
                Text = (index + 1).ToString(),
            };
            TexboxProperties(textBox);
            return textBox;
        }

        //Set the textbox properties.
        private void TexboxProperties(TextBox textBox)
        {
            ExVisualGridMetaData.TextBoxFormat textBoxFormat = ExVisualGridMetaData.RectGridNumFormat;
            textBox.BorderThickness = new Thickness(textBoxFormat.Thickness);
            textBox.FontSize = textBoxFormat.FontSize;
            textBox.Background = new SolidColorBrush(ColorUtils.getColor(textBoxFormat.FillColorString));
            textBox.Foreground = new SolidColorBrush(ColorUtils.getColor(textBoxFormat.ForegroundColorString));
            textBox.HorizontalAlignment = HorizontalAlignment.Center;
            textBox.VerticalAlignment = VerticalAlignment.Center;
            textBox.HorizontalContentAlignment = HorizontalAlignment.Center;
            textBox.VerticalContentAlignment = VerticalAlignment.Center;
            textBox.IsReadOnly = true;
        }

        protected override void OnDeactivated(EventArgs e)
        {
            base.OnDeactivated(e);
        }

        /// <summary>
        /// @yosef   3/1/20  marked
        /// delegate for the MouseLeftButtonDown
        /// 1) detect the following event)
        ///     (1) select visual grid type
        ///     (2) select visual grid size
        ///     (3) select import data from excel
        ///     if one of the above actions matched, it will perform that action
        ///     other wise continue to 2)
        /// 2) perform the window-move action
        /// 
        /// </summary>
        /// <param name="e"></param>
        protected override void OnMouseLeftButtonDown(MouseButtonEventArgs e)
        {
            if (prevSelRectRowNum != 0 || prevSelRectColNum != 0)
            {
                selHoldOn = true;
            }
            else
            {
                isMoving = true;
                Point relatedPos = e.GetPosition(this);
                moveStartPos = this.PointToScreen(relatedPos);
                moveStartWindowPos.X = this.Left;
                moveStartWindowPos.Y = this.Top;
            }
            base.OnMouseLeftButtonDown(e);
        }

        /// <summary>
        /// @yosef   3/1/20  marked
        /// delegate for MouseMove
        /// </summary>
        /// <param name="e"></param>
        protected override void OnMouseMove(MouseEventArgs e)
        {
            if (isMoving)
            {
                Point movedPos = this.PointToScreen(e.GetPosition(this));
                Vector delta = movedPos - moveStartPos;
                this.Left = moveStartWindowPos.X + delta.X;
                this.Top = moveStartWindowPos.Y + delta.Y;
            }

            if (!selHoldOn)
            {
                Point relatePosToRectGrid = e.GetPosition(vgSizeGrid);
                HighlightRectGrid(relatePosToRectGrid);
            }

            base.OnMouseMove(e);
        }

        /// <summary>
        /// @yosef   3/1/20  marked
        /// delegate for the MouseLeftButtonUp
        /// 1) detect the following event
        ///     (1) select the visual grid type
        ///     (2) select the visual grid size
        ///     (3) select import data from excel
        ///     if one of the above actions matched, it will performs the action
        ///     otherwise the window
        /// @yosef  3/1/20  fixed
        /// 1) invoke visualgrid.prepareVisualGrid()
        /// </summary>
        /// <param name="e"></param>
        protected override void OnMouseLeftButtonUp(MouseButtonEventArgs e)
        {
            if (isMoving)
            {
                isMoving = false;
            }

            // if mouse down on the cell of the grid-rect
            if (selHoldOn)
            {
                selHoldOn = false;

                // calculate the new selected row and cols
                Point relatedPos = e.GetPosition(vgSizeGrid);

                int curSelRectRowNum;
                int curSelRectColNum;
                if (relatedPos.X < 0 || relatedPos.Y < 0 || relatedPos.X > vgSizeGrid.Width || relatedPos.Y > vgSizeGrid.Height)
                {
                    curSelRectRowNum = 0;
                    curSelRectColNum = 0;
                }
                else
                {
                    curSelRectRowNum = Math.Min((int)(relatedPos.Y / vgSizeGrid.Height * ExVisualGridMetaData.MAX_ROW) + 1,
                        ExVisualGridMetaData.MAX_ROW);
                    curSelRectColNum = Math.Min((int)(relatedPos.X / vgSizeGrid.Width * ExVisualGridMetaData.MAX_COL) + 1,
                        ExVisualGridMetaData.MAX_COL);
                }

                // if the selected rows and cols of up-event not equals to down-event
                if (curSelRectRowNum != prevSelRectRowNum || curSelRectColNum != prevSelRectColNum)
                {
                    HighlightRectGrid(relatedPos);
                }
                else // global state tracker jumps into the visual grid instance
                {
                    ExVisualGridManager.Instance.prepareVisualGrid(
                        ExVisualGridPreviewManager.Instance.getVisualGridType(0),
                        curSelRectColNum, curSelRectRowNum);
                   
                    
                    PassDimentionToShapes(curSelRectColNum, curSelRectRowNum);
                }
            }

            //base.OnMouseLeftButtonUp(e);
        }

        private void PassDimentionToShapes(int columns, int rows)
        {
            columnsGridnumber.Text = columns.ToString();
            rowsGridnumber.Text = rows.ToString();
            EnabledOrDsabledInsertButton();
        }

        /// <summary>
        /// Pass the grid dimensions to the grid controller method
        /// </summary>
        public void SetDimention(int columns, int rows)
        {
            GridController.instance.SetColumnsAndRowsValue(columns, rows);
        }

        /// <summary>
        /// @yosef   3/1/20  marked
        /// Highlight RectGrid
        /// </summary>
        /// <param name="relatedPos">Mouse position relating to the RectGrid</param>
        private void HighlightRectGrid(Point relatedPos)
        {
            // @yosef - 12/25/19
            // ToDo 1: Highlight the rect(s) in the grid
            // ToDo 1 - 1: calc the row & col counts to highlight
            int curSelRectRowNum = 0;
            int curSelRectColNum = 0;

            if (relatedPos.X < 0 || relatedPos.Y < 0 || relatedPos.X > vgSizeGrid.Width || relatedPos.Y > vgSizeGrid.Height)
            {
                curSelRectRowNum = 0;
                curSelRectColNum = 0;
            }
            else
            {
                curSelRectRowNum = Math.Min((int)(relatedPos.Y / vgSizeGrid.Height * ExVisualGridMetaData.MAX_ROW) + 1,
                    ExVisualGridMetaData.MAX_ROW);
                curSelRectColNum = Math.Min((int)(relatedPos.X / vgSizeGrid.Width * ExVisualGridMetaData.MAX_COL) + 1,
                    ExVisualGridMetaData.MAX_COL);
            }

            if (curSelRectRowNum == prevSelRectRowNum && curSelRectColNum == prevSelRectColNum)
            {
                return;
            }
            // ToDo 1 - 2: unhighlight the prev rects and highlights new rects            
            ExVisualGridMetaData.RectFormat gridRectFormat = ExVisualGridMetaData.RectGridFormat;
            for (int i = 0; i < ExVisualGridMetaData.MAX_ROW; ++i)
            {
                for (int j = 0; j < ExVisualGridMetaData.MAX_COL; ++j)
                {
                    UnHighlightRect(gridRect[i][j]);
                }
            }

            for (int i = 0; i < curSelRectRowNum; ++i)
            {
                for (int j = 0; j < curSelRectColNum; ++j)
                {
                    HighlightRect(gridRect[i][j]);
                }
            }

            // ToDo 1 - 3: Unhighlight the prev textboxs and highlights new textboxs
            for (int i = 0; i < curSelRectRowNum; ++i)
            {
                HighlightTextBox(rowNumText[i]);
            }
            for (int i = curSelRectRowNum; i < ExVisualGridMetaData.MAX_ROW; ++i)
            {
                UnHighlightTextBox(rowNumText[i]);
            }

            for (int j = 1; j < curSelRectColNum; ++j)
            {
                HighlightTextBox(colNumText[j]);
            }

            for (int j = Math.Max(curSelRectColNum, 1); j < ExVisualGridMetaData.MAX_COL; ++j)
            {
                UnHighlightTextBox(colNumText[j]);
            }

            // ToDo 1 - 4: Reset the prev selected row and col
            prevSelRectRowNum = curSelRectRowNum;
            prevSelRectColNum = curSelRectColNum;
        }

        /// <summary>
        /// @yosef   3/1/20  marked
        /// Unhighlight Rectangle; perform this action to unselected rectangles
        /// </summary>
        /// <param name="rect">Reference to Rectangle to Unhighlight</param>
        private void UnHighlightRect(Rectangle rect)
        {
            // @yosef, 26/12/19
            ExVisualGridMetaData.RectFormat rectFormat = ExVisualGridMetaData.RectGridFormat;
            rect.Stroke = new SolidColorBrush(ColorUtils.getColor(rectFormat.StrokeColorString));
            rect.Fill = new SolidColorBrush(ColorUtils.getColor(rectFormat.FillColorString));
        }

        /// <summary>
        /// @yosef   3/1/20  marked
        /// Highlight Rectangle; perform this action to selected rectangle
        /// </summary>
        /// <param name="rect">Reference to Rectangle to Highlight</param>
        private void HighlightRect(Rectangle rect)
        {
            // @yosef, 26/12/19
            ExVisualGridMetaData.RectFormat rectFormat = ExVisualGridMetaData.RectGridFormat;
            rect.Stroke = new SolidColorBrush(ColorUtils.getColor(rectFormat.HighlightStrokeColorString));
            rect.Fill = new SolidColorBrush(ColorUtils.getColor(rectFormat.HighlightFillColorString));
        }

        /// <summary>
        /// @yosef   3/1/20  marked
        /// Highlight TextBox; perform this action to selected textbox(s)
        /// </summary>
        /// <param name="textBox">Reference to TextBox to Highlight</param>
        private void HighlightTextBox(TextBox textBox)
        {
            // @yosef, 26/12/19
            ExVisualGridMetaData.TextBoxFormat rectNumFormat = ExVisualGridMetaData.RectGridNumFormat;
            textBox.Foreground = new SolidColorBrush(ColorUtils.getColor(rectNumFormat.HighlightForegroundColorString));
            //textBox.Background = new SolidColorBrush(ColorUtils.getColor(rectNumFormat.HighlightFillColorString));
        }

        /// <summary>
        /// @yosef   3/1/20  marked
        /// Unhighlight TextBox; perform this action to unselected textbox(s)
        /// </summary>
        /// <param name="textBox">Reference to TextBox to Unhighlight</param>
        private void UnHighlightTextBox(TextBox textBox)
        {
            // @yosef, 26/12/19
            ExVisualGridMetaData.TextBoxFormat rectNumFormat = ExVisualGridMetaData.RectGridNumFormat;
            textBox.Foreground = new SolidColorBrush(ColorUtils.getColor(rectNumFormat.ForegroundColorString));
            //textBox.Background = new SolidColorBrush(ColorUtils.getColor(rectNumFormat.FillColorString));
        }

        private void CancelGridCreation(object sender, EventArgs e)
        {
            Close();
        }

        /// <summary>
        /// Send the columns and rows values entry by user to the SetDimention method
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridDimentions(object sender, RoutedEventArgs e)
        {            
            GridController.instance.CreateGrid(getSelectedColumns(), getSelectedRows());
            Close();
        }

        private int getSelectedColumns()
        {
            return sanitizeDimention(columnsGridnumber.Text);
        }

        private int getSelectedRows()
        {
            return sanitizeDimention(rowsGridnumber.Text);
        }

        private int sanitizeDimention(string dimention)
        {
            try
            {
                //TODO: Control Max value and Minimun value
                return Convert.ToInt32(dimention);
            }
            catch (Exception e)
            {
                return 1;
            }
        }



        private void EnabledOrDsabledInsertButton()
        {
            //validate if the colums and rows value entry by user are bigger than zero
            if (Convert.ToInt32(columnsGridnumber.Text) > 0 && Convert.ToInt32(rowsGridnumber.Text) > 0)
                passGridDimentions.IsEnabled = true;
            else
                passGridDimentions.IsEnabled = false;
        }

        /// <summary>
        /// This method force only number values pass to shape textbox.
        /// This method is called into 'PreviewTextInput' event when the user try to insert a value into textbox shape.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void NumberValidation(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]");
            e.Handled = regex.IsMatch(e.Text);
        }

        private void ShapeTextBoxLostFocus(object sender, RoutedEventArgs e)
        {
            if ("" == columnsGridnumber.Text) columnsGridnumber.Text = "0";
            if ("" == rowsGridnumber.Text) rowsGridnumber.Text = "0";
            EnabledOrDsabledInsertButton();
        }
    }
}