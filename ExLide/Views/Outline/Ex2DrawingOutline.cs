﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ExLide.Controllers;
using ExLide.Model.FSM;
using ExLide.Model.Outline;
using ExLide.Model.PowerPoint;
using Gma.System.MouseKeyHook;
using Gma.System.MouseKeyHook.WinApi;
using Microsoft.Office.Interop.PowerPoint;

namespace ExLide.Views.Outline
{
    /// <summary>
    /// @yosef  29/12/19
    /// Draw Drawing Outline
    /// </summary>
    public class Ex2DrawingOutline
    {
        #region define Singleton Pattern

        /// <summary>
        /// @yosef  29/12/19
        /// instance of Ex2DrawingOutline
        /// </summary>
        private static Ex2DrawingOutline instance = null;

        /// <summary>
        /// @yosef  29/12/19
        /// private constructor
        /// </summary>
        private Ex2DrawingOutline()
        {
            GridController = GridController.instance;
        }

        /// <summary>
        /// @yosef  29/12/19
        /// get instance of Ex2DrawingOutline
        /// </summary>
        public static Ex2DrawingOutline Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new Ex2DrawingOutline();
                }
                return instance;
            }
        }

        #endregion

        #region define variables
        /// var(s) defined here should be deleted in the future.
        /// 
        //Shape temp_posText = null;
        bool createGrid = false;
        GridController GridController;
        ExDrawingOutline drawingOutline = ExDrawingOutline.Instance;
        IKeyboardMouseEvents m_GlobalHook = null;

        #endregion

        #region manage drawing(s)

        /// <summary>
        /// @yosef  29/12/19
        /// </summary>
        public void refresh()
        {
            Slide selectedSlide = ExSelectedSlide.Instance.GetSlideRef();
            System.Drawing.Point relatedPos = ExSlidePane.Instance.getPositionInPane();
            switch (ExDrawingOutlineFSM.Instance.getCurrentState())
            {
                case ExDrawingOutlineFSM.ExDrawingOutlineState.DOLS_NONE:
                    if (null != drawingOutline.Outline)
                    {
                        drawingOutline.Outline.Delete();
                        drawingOutline.Outline = null;
                    }
                    break;
                case ExDrawingOutlineFSM.ExDrawingOutlineState.DOLS_HIDDEN:

                    break;
                case ExDrawingOutlineFSM.ExDrawingOutlineState.DOLS_PREFERED:
                    if (null == drawingOutline.Outline)
                    {
                        m_GlobalHook = Hook.GlobalEvents();
                        m_GlobalHook.MouseDownExt += MouseDownExt;
                        m_GlobalHook.MouseUpExt += MouseUpExt;
                        createGrid = true;
                    }
                    else
                    {
                        drawingOutline.Outline.Left = relatedPos.X;
                        drawingOutline.Outline.Top = relatedPos.Y;
                    }
                    break;
                case ExDrawingOutlineFSM.ExDrawingOutlineState.DOLS_DRAW:
                    Cursor.Current = Cursors.Cross;
                    break;
            }
        }

        /// <summary>
        /// When user insert the grid this event get the press button on the mouse.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MouseDownExt(object sender, MouseEventArgs e)
        {
            if (null != drawingOutline.Outline)
            {
                switch (e.Button)
                {
                    case MouseButtons.Left:
                        Cursor.Current = Cursors.Cross;
                        createGrid = true;
                        drawingOutline.Outline.Delete();
                        drawingOutline.Outline = null;
                        break;
                    case MouseButtons.None:
                        break;
                    case MouseButtons.Right:
                        createGrid = false;
                        break;
                    case MouseButtons.Middle:
                        break;
                    case MouseButtons.XButton1:
                        break;
                    case MouseButtons.XButton2:
                        break;
                }
            }
        }

        /// <summary>
        /// This method obtain the last pointer position into the slide.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MouseUpExt(object sender, MouseEventArgs e)
        {
            if (createGrid)
            {
                System.Drawing.Point finalPosition = ExSlidePane.Instance.getPositionInPane();
                if (finalPosition.X >= 0 && finalPosition.Y >= 0)
                    //GridController.CreateGrid(drawingOutline.Left, drawingOutline.Top, Convert.ToInt16(finalPosition.X), Convert.ToInt16(finalPosition.Y));
                createGrid = false;
            }
            //ExGlobalMouseHookController.Instance.SwitchHook(ExGlobalMouseHookController.HOOK_STAT.HOOK_OUTLINE);
            m_GlobalHook.MouseDownExt -= MouseDownExt;
            m_GlobalHook.MouseUpExt -= MouseUpExt;
            m_GlobalHook.Dispose();
        }
        #endregion
    }
}
